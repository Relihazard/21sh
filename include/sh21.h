/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh21.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/29 14:02:45 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 10:05:00 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH21_H
# define SH21_H

/*
** Includes for authorized functions
** stdlib.h for malloc(3), free(3), exit(3) and getenv(3)
** unistd.h for access(2), close(2), read(2), write(2), getcwd(3), chdir(2),
** fork(2), execve(2), pipe(2), dup(2), dup2(2), isatty(3),
** ttyname(3) and ttyslot(3)
** fcntl.h for open(2)
** dirent.h for opendir(3), readdir(3) and closedir(3)
** sys/stat.h for stat(2), lstat(2) and fstat(2)
** sys/wait.h for wait(2), waitpid(2), wait3(2) and wait4(2)
** signal.h for signal(3) and kill(2)
** sys/ioctl.h for iotctl(2)
** termios.h for tcsetattr(3) and tcgetattr(3)
** curses.h and term.h for tgetent(3X), tgetflag(3X), tgetnum(3X), tgetstr(3X),
** tgoto(3X) and tputs(3X)
** libft.h for the libft functions
*/
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <dirent.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <signal.h>
# include <sys/ioctl.h>
# include <termios.h>
# include <curses.h>
# include <term.h>
# include <libft.h>

# define PROG_NAME			"21sh"

# define HISTORY_FILE		".21sh_history"
# define HISTORY_MAXSIZE	1000
# define HEREDOC_FILE		".21sh_heredoc"

# define E_ACCES			1
# define E_NOENT			2
# define E_2BIG				3
# define E_NOTDIR			4
# define E_NOMEM			5
# define E_UNKNO			256

typedef enum	e_token_type
{
	T_NONE,
	T_NEWLINE,
	T_EOF,
	T_AND,
	T_AMPERSAND,
	T_OR,
	T_PIPE,
	T_SEMICOLON,
	T_DLESSDASH,
	T_DLESS,
	T_LESSGREAT,
	T_LESSAND,
	T_LESS,
	T_DGREAT,
	T_GREATAND,
	T_CLOBBER,
	T_GREAT,
	T_IO_NUMBER,
	T_WORD
}				t_token_type;

typedef enum	e_node_type
{
	N_PIPE,
	N_SEP,
	N_SEPAND,
	N_REDIRECTION,
	N_AND,
	N_OR,
	N_COMMAND
}				t_node_type;

typedef enum	e_state
{
	S_GENERAL,
	S_EXIT_SUCCESS = S_GENERAL,
	S_EXIT_FAILURE = 1,
	S_OPERATOR,
	S_QUOTE,
	S_DQUOTE,
	S_HEREDOC,
	S_COPY
}				t_state;

typedef enum	e_redir_type
{
	R_LESS,
	R_LESSAND,
	R_GREAT,
	R_GREATAND,
	R_DGREAT,
	R_LESSGREAT,
	R_CLOBBER,
	R_DLESS,
	R_DLESSDASH
}				t_redir_type;

typedef enum	e_key_type
{
	K_UP,
	K_DOWN,
	K_RIGHT,
	K_LEFT,
	K_CTRL_UP,
	K_CTRL_DOWN,
	K_CTRL_RIGHT,
	K_CTRL_LEFT,
	K_BACKSPACE,
	K_ENTER,
	K_CTRL_D,
	K_HOME,
	K_END,
	K_OTHER,
	K_OPT_V,
	K_OPT_P,
	K_NONE
}				t_key_type;

typedef struct	s_token
{
	t_token_type		type;
	char				*value;
	struct s_token		*next;
}				t_token;

typedef struct	s_lexer
{
	size_t			tokenc;
	t_token			*list;
	t_state			state;
	unsigned int	cchar;
	unsigned int	tcchar;
}				t_lexer;

typedef struct s_ast	t_ast;

typedef struct	s_parser
{
	t_lexer			*lexer;
	unsigned int	tree_pos;
	unsigned int	tree_size;
	t_ast			**tree;
	int				error;
}				t_parser;

typedef struct	s_command_node
{
	char	**prefix;
	char	**argv;
	int		argc;
}				t_command_node;

typedef struct	s_binary_node
{
	t_ast		*lhs;
	t_ast		*rhs;
}				t_binary_node;

typedef struct	s_redir_node
{
	size_t			size;
	t_redir_type	*type;
	int				*fd;
	char			**word;
	t_ast			*mhs;
}				t_redir_node;

typedef union	u_node
{
	t_command_node	command_node;
	t_redir_node	redir_node;
	t_binary_node	and_node;
	t_binary_node	or_node;
	t_binary_node	sep_node;
	t_binary_node	sepand_node;
	t_binary_node	pipe_node;
}				t_node;

struct			s_ast
{
	t_node_type		type;
	t_node			node;
};

typedef struct	s_cursor
{
	unsigned int	cchar;
	unsigned int	line;
}				t_cursor;

typedef struct	s_key
{
	t_key_type	type;
	char		value[6];
}				t_key;

typedef struct	s_dlist
{
	void			*content;
	size_t			content_size;
	struct s_dlist	*next;
	struct s_dlist	*prev;
}				t_dlist;

typedef struct	s_history
{
	int		csize;
	int		current_id;
	t_dlist	*list;
	char	*location;
}				t_history;

typedef struct	s_redir_context
{
	int		old;
	int		save;
}				t_redir_context;

typedef struct	s_shell
{
	struct winsize	screen_size;
	char			*prompt;
	char			*line;
	t_history		*history;
	t_cursor		*cursor;
	t_key			*key;
	char			*buf_copy;
	unsigned int	copy_char;
	t_lexer			*lexer;
	t_ast			*ast;
	unsigned int	prompt_len;
	t_state			state;
	int				status;
	t_parser		*parser;
	char			*hline;
	char			*last_line;
	char			*heredoc_buf;
}				t_shell;

typedef struct	s_kcheck_tab
{
	t_key_type		type;
	int				(*f)(char *);
}				t_kcheck_tab;

typedef struct	s_kexec_tab
{
	t_key_type		type;
	int				(*f)(t_shell *);
}				t_kexec_tab;

typedef struct	s_redir_tab
{
	t_redir_type	type;
	int				(*f)(t_redir_node *, int);
}				t_redir_tab;

typedef struct	s_parse_redtype
{
	t_token_type	ttype;
	t_redir_type	rtype;
	int				fd;
}				t_parse_redtype;

typedef struct	s_astfree_tab
{
	t_node_type	type;
	void		(*f)(t_ast *);
}				t_astfree_tab;

typedef struct	s_err_tab
{
	int			errnum;
	char		*str;
}				t_err_tab;

typedef struct	s_pipeline
{
	int		count;
	int		last;
	pid_t	*list;
}				t_pipeline;

typedef struct	s_builtin_tab
{
	char	*name;
	int		(*f)(char **);
}				t_builtin_tab;

extern char				**g_environ;
extern t_shell			*g_shell;

int				ft_loop(t_shell *shell);

t_token			*ft_inittoken(size_t size);
void			ft_addtoken(t_lexer **head, t_token *new);
void			ft_freetoken(t_token **token);

void			ft_freeast(t_ast *tree);

t_token			*ft_lnext_token(t_lexer *lexer);
int				ft_buildlexer(t_shell *shell);
void			ft_freelexer(t_lexer **lexer);

int				ft_tokenization(t_lexer **lexer, t_token **token,
				char *input, size_t size);

t_ast			*ft_parse(t_parser *parser);

int				ft_evaluate(t_ast *tree);

int				ft_isoperator(char c);
int				ft_canformoperator(char *value, char c);
int				ft_getoperator(char *value);

int				ft_termios(void);

int				ft_screen(t_shell *shell);

int				ft_exit(t_cursor **cursor, char **line, int status);

int				ft_hadd_line(t_shell *shell);
int				ft_init_history(t_shell *shell);
void			ft_dlist_free_last(t_dlist **head);

t_key			*ft_key_get(char *buffer);
t_key			*ft_key_init(t_key_type type, char *buffer);
int				ft_key_exec(t_shell *shell);

int				ft_kcheck_up(char *buffer);
int				ft_kcheck_down(char *buffer);
int				ft_kcheck_right(char *buffer);
int				ft_kcheck_left(char *buffer);
int				ft_kcheck_ctrlup(char *buffer);
int				ft_kcheck_ctrldown(char *buffer);
int				ft_kcheck_ctrlright(char *buffer);
int				ft_kcheck_ctrleft(char *buffer);
int				ft_kcheck_backspace(char *buffer);
int				ft_kcheck_enter(char *buffer);
int				ft_kcheck_ctrld(char *buffer);
int				ft_kcheck_home(char *buffer);
int				ft_kcheck_end(char *buffer);
int				ft_kcheck_optv(char *buffer);
int				ft_kcheck_optp(char *buffer);

int				ft_kexec_up(t_shell *shell);
int				ft_kexec_down(t_shell *shell);
int				ft_kexec_right(t_shell *shell);
int				ft_kexec_left(t_shell *shell);
int				ft_kexec_ctrlup(t_shell *shell);
int				ft_kexec_ctrldown(t_shell *shell);
int				ft_kexec_ctrlright(t_shell *shell);
int				ft_kexec_ctrleft(t_shell *shell);
int				ft_kexec_backspace(t_shell *shell);
int				ft_kexec_enter(t_shell *shell);
int				ft_kexec_ctrld(t_shell *shell);
int				ft_kexec_end(t_shell *shell);
int				ft_kexec_home(t_shell *shell);
int				ft_kexec_other(t_shell *shell);
int				ft_kexec_optv(t_shell *shell);
int				ft_kexec_optp(t_shell *shell);
int				ft_kexec_copy_right(t_shell *shell);
int				ft_kexec_copy_left(t_shell *shell);
int				ft_kexec_copy_ctrlup(t_shell *shell);
int				ft_kexec_copy_ctrldown(t_shell *shell);
int				ft_kexec_copy_ctrlright(t_shell *shell);
int				ft_kexec_copy_ctrleft(t_shell *shell);
int				ft_kexec_copy_end(t_shell *shell);
int				ft_kexec_copy_home(t_shell *shell);

int				ft_init_prompt(t_shell *shell);

t_dlist			*ft_dlist_new(void *content, size_t content_size);
int				ft_kexec_end(t_shell *shell);
int				ft_kexec_other(t_shell *shell);

int				ft_init_prompt(t_shell *shell);

t_dlist			*ft_dlist_new(void *content, size_t content_size);
void			ft_dlist_add(t_dlist **head, t_dlist *new);
void			ft_dlist_free(t_dlist **head);

t_ast			*ft_acreate_sepand(t_ast *lhs, t_ast *rhs);
t_ast			*ft_acreate_sep(t_ast *lhs, t_ast *rhs);
t_ast			*ft_acreate_and(t_ast *lhs, t_ast *rhs);
t_ast			*ft_acreate_or(t_ast *lhs, t_ast *rhs);
t_ast			*ft_acreate_pipe(t_ast *lhs, t_ast *rhs);
t_ast			*ft_acreate_command(void);
t_ast			*ft_acreate_redirection(void);

void			ft_acmd_addargv(t_ast *cmd, char *argv);
void			ft_ared_add(t_ast *node, t_redir_type type, int fd, char *word);

void			ft_afree_sepand(t_ast *node);
void			ft_afree_sep(t_ast *node);
void			ft_afree_and(t_ast *node);
void			ft_afree_or(t_ast *node);
void			ft_afree_pipe(t_ast *node);
void			ft_afree_command(t_ast *node);
void			ft_afree_redirection(t_ast *node);

void			ft_afree_sepand_node(t_ast *node);
void			ft_afree_sep_node(t_ast *node);
void			ft_afree_and_node(t_ast *node);
void			ft_afree_or_node(t_ast *node);
void			ft_afree_pipe_node(t_ast *node);
void			ft_afree_command_node(t_ast *node);
void			ft_afree_redirection_node(t_ast *node);

void			ft_aexec_sepand(t_binary_node *node);
void			ft_aexec_sep(t_binary_node *node);
void			ft_aexec_and(t_binary_node *node);
void			ft_aexec_or(t_binary_node *node);
void			ft_aexec_pipe(t_binary_node *node);
void			ft_aexec_command(t_command_node *node);
void			ft_aexec_redirection(t_redir_node *node);

void			ft_pfree(t_parser *parser);

void			ft_signal_handler_cmd(int signo);
void			ft_signal_handler_shell(int signo);
void			ft_signal_handler_heredoc(int signo);

char			*ft_strerror(int errnum);

t_ast			*ft_parse_pipeline(t_parser *parser);
int				ft_parse_element(t_parser *parser, t_ast *cmd, t_ast **red);
void			ft_parse_redirection(t_parser *parser, t_ast **red);
void			ft_parse_error(t_parser *parser, t_token *token);
t_ast			*ft_padd_node(t_parser *parser, t_ast *node);
t_ast			*ft_parse_andor(t_parser *parser);

pid_t			ft_exec_pipe_left(int p[2], t_ast *left, t_pipeline *pipeline);
pid_t			ft_exec_pipe_right(int p[2], t_ast *r, t_pipeline *pipeline);
char			*ft_get_binary(char *argv);

int				ft_apply_rdless(t_redir_node *node, int i);
int				ft_apply_rless(t_redir_node *node, int i);
int				ft_apply_rgreat(t_redir_node *node, int i);
int				ft_apply_rdgreat(t_redir_node *node, int i);
int				ft_apply_rlessgreat(t_redir_node *node, int i);
int				ft_apply_rlessand(t_redir_node *node, int i);

int				ft_red_error(void);

int				ft_bcd(char **argv);
int				ft_becho(char **argv);
int				ft_benv(char **argv);
int				ft_bexit(char **argv);
int				ft_bsetenv(char **argv);
int				ft_bunsetenv(char **argv);

char			**ft_envrealloc(size_t new_size);
void			ft_initenv(char **environ);

int				ft_wordexp(t_token *list);

int				ft_builtin(char **argv);

char			*ft_getenv(char *var);

void			ft_key_move_beg(t_shell *shell);

#endif
