# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/29 13:56:59 by agrossma          #+#    #+#              #
#    Updated: 2018/11/19 12:38:59 by agrossma         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SHELL		:= /bin/bash

################################################################################
# Start of system configuration section                                        #
################################################################################

NAME		:= 21sh
CC			:= gcc
CFLAGS		+= -Wall -Wextra -Werror
MKDIR		:= mkdir -p
RM			:= rm -f
RMDIR		:= rmdir
ECHO		:= echo
QUIET		:= @
MAKE		:= make

################################################################################
# End of system configuration section                                          #
################################################################################

################################################################################
# Start of files definition section                                            #
################################################################################

_INCLUDE	:= include/
CFLAGS		+= -I$(_INCLUDE)
_SRC		:= src/
SRC			:= \
	ast.apply_redirection.c				\
	ast.ast.c							\
	ast.command.c						\
	ast.create_redirection.c			\
	ast.error.c							\
	ast.free_command.c					\
	ast.free_sepand.c					\
	ast.free_sepand_node.c				\
	ast.evaluate.c						\
	ast.heredoc.c						\
	ast.pipe.c							\
	ast.pipeline.c						\
	ast.redirection.c					\
	ast.separator.c						\
	builtin.cd.c						\
	builtin.echo.c						\
	builtin.env.c						\
	builtin.exit.c 						\
	builtin.setenv.c					\
	builtin.unsetenv.c					\
	error.strerror.c					\
	history.dlist.c						\
	history.history.c					\
	key.check_arrow.c					\
	key.check_arrow.ctrl.c				\
	key.check_copy_paste.c				\
	key.check_write.c					\
	key.check.c							\
	key.exec_arrow.c					\
	key.exec_arrow.copy_paste.c			\
	key.exec_arrow.ctrl.c				\
	key.exec_arrow.ctrl.copy_paste.c	\
	key.exec_copy_paste.c				\
	key.exec_enter.c					\
	key.exec_write.c					\
	key.exec.c							\
	lexer.lexer.c						\
	lexer.token.c						\
	lexer.tokenization.c				\
	lexer.util.c						\
	lexer.wordexp.c						\
	main.c								\
	parser.error.c						\
	parser.parse_element.c				\
	parser.parse_pipeline.c				\
	parser.parser.c						\
	shell.loop.c						\
	shell.prompt.c						\
	signal.signal.c						\
	termios.screen.c					\
	termios.termios.c
_OBJ		:= obj/
OBJ			+= $(addprefix $(_OBJ), $(SRC:.c=.o))

################################################################################
# End of files definition section                                              #
################################################################################

################################################################################
# Start of libraries definition section                                        #
################################################################################

_LIBFT		:= libft/
CFLAGS		+= -I$(_LIBFT)$(_INCLUDE)
LIBFT_A		:= libft.a
LIBFT		:= ft
LIBTERMCAP	:= termcap

################################################################################
# End of libraries definition section                                          #
################################################################################

################################################################################
# Start of linking configuration section                                       #
################################################################################

LD			:= gcc
LDFLAGS		:= -L$(_LIBFT)
LDLIBS		:= -l$(LIBFT) -l$(LIBTERMCAP)

################################################################################
# End of linking configuration section                                         #
################################################################################

################################################################################
# Start of rules definition section                                            #
################################################################################

.PHONY: all clean fclean re

all: $(NAME)

$(NAME): $(_LIBFT)$(LIBFT_A) $(_OBJ) $(OBJ)
	$(QUIET)$(ECHO) "[$(NAME)]	LD	$@"
	$(QUIET)$(LD) $(OBJ) $(LDFLAGS) $(LDLIBS) -o $@

$(_LIBFT)$(LIBFT_A):
	$(QUIET)$(MAKE) -C $(_LIBFT)

$(_OBJ):
	$(QUIET)$(MKDIR) $(_OBJ)

$(_OBJ)%.o: $(_SRC)%.c
	$(QUIET)$(ECHO) "[$(NAME)]	CC	$(notdir $@)"
	$(QUIET)$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(QUIET)$(ECHO) "[$(NAME)]	RM	$(_OBJ)"
	$(QUIET)$(RM) $(OBJ)
	$(QUIET)if [ -d "$(_OBJ)" ]; then \
		$(RMDIR) $(_OBJ); \
	fi
	$(QUIET)$(MAKE) -C $(_LIBFT) $@

fclean: clean
	$(QUIET)$(ECHO) "[$(NAME)]	RM	$(NAME)"
	$(QUIET)$(RM) $(NAME)
	$(QUIET)$(MAKE) -C $(_LIBFT) $@

re: fclean all

################################################################################
# End of rules definition section                                              #
################################################################################
