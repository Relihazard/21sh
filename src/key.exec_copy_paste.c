/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.exec_copy_paste.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 00:10:57 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 10:07:58 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int				ft_kexec_copy_end(t_shell *shell)
{
	while (!ft_kexec_copy_right(shell))
		;
	return (0);
}

int				ft_kexec_copy_home(t_shell *shell)
{
	while (!ft_kexec_copy_left(shell))
		;
	return (0);
}

int				ft_kexec_optv(t_shell *shell)
{
	shell->state = S_COPY;
	shell->buf_copy = ft_strnew(1);
	shell->copy_char = shell->cursor->cchar;
	ft_memdel((void **)&(shell->key));
	return (0);
}

int				ft_kexec_optp(t_shell *shell)
{
	int		i;

	if (shell->state != S_GENERAL)
		return (1);
	shell->line = ft_insert(shell->line, shell->buf_copy, shell->cursor->cchar);
	ft_strdel(&shell->buf_copy);
	i = shell->cursor->line;
	while (--i)
		(void)tputs(tgoto(tgetstr("up", NULL), 0, 0), 0, &ft_iputchar);
	(void)tputs(tgoto(tgetstr("ch", NULL), 0, 0), 0, &ft_iputchar);
	(void)tputs(tgetstr("cd", NULL), 0, &ft_iputchar);
	(void)ft_printf("%s%s", shell->prompt, shell->line);
	shell->cursor->cchar = ft_strlen(shell->line);
	shell->cursor->line =
		1 + ((ft_strlen(shell->line) + shell->prompt_len)
		/ shell->screen_size.ws_col);
	ft_memdel((void **)&(shell->key));
	return (0);
}
