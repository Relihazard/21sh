/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.heredoc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 09:36:02 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/10 19:07:13 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static void	ft_heredoc_write(int fd, char *c, char nl)
{
	(void)write(STDOUT_FILENO, &nl, 1);
	(void)write(fd, c, ft_strlen(c));
	(void)write(fd, &nl, 1);
}

static void	ft_heredoc_char(char c)
{
	char	*join;
	char	buffer[2];

	(void)write(STDOUT_FILENO, &c, 1);
	buffer[0] = c;
	buffer[1] = 0;
	join = ft_strjoin(g_shell->hline, buffer);
	ft_strdel(&g_shell->hline);
	g_shell->hline = join;
}

static void	ft_heredoc_loop(char *delimiter, int fd)
{
	char	buffer[6];

	while (42)
	{
		(void)ft_memset((void *)buffer, 0, 6);
		(void)read(STDIN_FILENO, buffer, 6);
		if (g_shell->state == S_GENERAL)
		{
			g_shell->heredoc_buf = ft_strdup(buffer);
			return ;
		}
		if (ft_isprint(buffer[0]))
			ft_heredoc_char(buffer[0]);
		else if (buffer[0] == '\n')
		{
			if (ft_strlen(g_shell->hline) == ft_strlen(delimiter)
				&& ft_strncmp(delimiter,
				g_shell->hline, ft_strlen(delimiter)) == 0)
				return ;
			ft_heredoc_write(fd, g_shell->hline, '\n');
			ft_printf("%s", "heredoc> ");
			ft_memset((void *)g_shell->hline, 0, ft_strlen(g_shell->hline));
		}
	}
}

static int	ft_heredoc(char *delimiter)
{
	int		fd;

	if ((fd = open("./"HEREDOC_FILE, O_CREAT | O_TRUNC | O_WRONLY,
		S_IRUSR | S_IWUSR)) == -1)
		return (1);
	g_shell->state = S_HEREDOC;
	ft_printf("%s", "heredoc> ");
	g_shell->hline = ft_strnew(1);
	signal(SIGINT, &ft_signal_handler_heredoc);
	ft_heredoc_loop(delimiter, fd);
	ft_strdel(&g_shell->hline);
	close(fd);
	if (g_shell->state != S_HEREDOC)
		return (2);
	write(1, "\n", 1);
	if ((fd = open("./"HEREDOC_FILE, O_RDONLY)) == -1)
		return (1);
	dup2(fd, STDIN_FILENO);
	close(fd);
	g_shell->state = S_GENERAL;
	return (0);
}

int			ft_apply_rdless(t_redir_node *node, int i)
{
	int		ret;

	ret = ft_heredoc(node->word[i]);
	if (ret == 1)
		return (ft_red_error());
	else if (ret == 2)
		return (1);
	else
		return (0);
}
