/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.echo.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 17:13:53 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 10:59:36 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int			ft_becho(char **argv)
{
	int		i;
	int		has_out;

	i = 1;
	has_out = 0;
	while (argv[i])
	{
		if (has_out && argv[i + 1])
			(void)write(1, " ", 1);
		has_out = ft_printf("%s", argv[i]);
		i++;
	}
	(void)write(1, "\n", 1);
	return (0);
}
