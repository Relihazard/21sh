/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.exec_arrow.ctrl.copy_paste.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 10:06:32 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 10:07:06 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int				ft_kexec_copy_ctrlup(t_shell *shell)
{
	int		i;
	int		j;

	i = shell->cursor->cchar;
	j = shell->cursor->cchar;
	if (j >= shell->screen_size.ws_col)
		j -= shell->screen_size.ws_col;
	else
		j = 0;
	while (i-- > j)
		ft_kexec_copy_left(shell);
	ft_memdel((void **)&(shell->key));
	return (0);
}

int				ft_kexec_copy_ctrldown(t_shell *shell)
{
	size_t	len;
	int		i;
	int		j;

	i = shell->cursor->cchar;
	j = shell->cursor->cchar;
	len = ft_strlen(shell->line);
	if (len >= shell->screen_size.ws_col)
		j += shell->screen_size.ws_col;
	else
		j = len;
	while (i++ < j)
		ft_kexec_copy_right(shell);
	ft_memdel((void **)&(shell->key));
	return (0);
}

int				ft_kexec_copy_ctrlright(t_shell *shell)
{
	size_t	len;

	len = ft_strlen(shell->line);
	while (shell->cursor->cchar < len
		&& !ft_isspace((shell->line)[shell->cursor->cchar]))
		ft_kexec_copy_right(shell);
	while (shell->cursor->cchar < len
		&& ft_isspace((shell->line)[shell->cursor->cchar]))
		ft_kexec_copy_right(shell);
	ft_memdel((void **)&(shell->key));
	return (0);
}

int				ft_kexec_copy_ctrleft(t_shell *shell)
{
	while (shell->cursor->cchar > 0
		&& !ft_isspace((shell->line)[shell->cursor->cchar]))
		ft_kexec_copy_left(shell);
	while (shell->cursor->cchar > 0
		&& ft_isspace((shell->line)[shell->cursor->cchar]))
		ft_kexec_copy_left(shell);
	while (shell->cursor->cchar > 0
		&& !ft_isspace((shell->line)[shell->cursor->cchar]))
		ft_kexec_copy_left(shell);
	if (shell->cursor->cchar > 0)
		ft_kexec_copy_right(shell);
	ft_memdel((void **)&(shell->key));
	return (0);
}
