/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.signal.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 18:43:52 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 08:51:57 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

void	ft_signal_handler_heredoc(int signo)
{
	if (signo == SIGINT)
	{
		ft_strdel(&g_shell->hline);
		(void)ft_printf("^C\n%s", g_shell->prompt);
		g_shell->state = S_GENERAL;
		signal(signo, &ft_signal_handler_shell);
	}
}

void	ft_signal_handler_cmd(int signo)
{
	if (signo == SIGINT)
	{
		signal(signo, &ft_signal_handler_shell);
		(void)ft_printf("^C\n");
	}
}

void	ft_signal_handler_shell(int signo)
{
	if (signo == SIGINT)
	{
		ft_strdel(&g_shell->line);
		ft_strdel(&g_shell->last_line);
		ft_strdel(&g_shell->buf_copy);
		g_shell->line = ft_strnew(1);
		g_shell->cursor->cchar = 0;
		g_shell->state = S_GENERAL;
		g_shell->cursor->line = 1;
		(void)ft_printf("^C\n%s", g_shell->prompt);
		signal(signo, &ft_signal_handler_shell);
	}
	else if (signo == SIGWINCH)
	{
		(void)ioctl(0, TIOCGWINSZ, &g_shell->screen_size);
		g_shell->cursor->line =
			1 + ((g_shell->prompt_len + g_shell->cursor->cchar)
			/ g_shell->screen_size.ws_col);
		signal(signo, &ft_signal_handler_shell);
	}
}
