/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.dlist.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 16:15:58 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:08:37 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

void		ft_dlist_free_last(t_dlist **head)
{
	t_dlist	*tmp;

	if (*head == NULL)
		return ;
	if ((*head)->next == NULL)
	{
		ft_memdel(&((*head)->content));
		ft_memdel((void **)&(*head));
		return ;
	}
	tmp = *head;
	while (tmp->next && tmp->next->next)
		tmp = tmp->next;
	ft_memdel(&(tmp->next->content));
	ft_memdel((void **)&(tmp->next));
	tmp->next = NULL;
}

t_dlist		*ft_dlist_new(void *content, size_t content_size)
{
	t_dlist		*new;

	if ((new = (t_dlist *)ft_memalloc(sizeof(t_dlist))) == NULL)
		return (NULL);
	new->content = ft_memalloc(content_size);
	(void)ft_memcpy(new->content, content, content_size);
	new->content_size = content_size;
	return (new);
}

void		ft_dlist_free(t_dlist **head)
{
	t_dlist	*tmp;

	while (*head)
	{
		tmp = (*head)->next;
		ft_memdel(&(*head)->content);
		ft_memdel((void **)&(*head));
		*head = tmp;
	}
	ft_memdel(&(*head)->content);
	ft_memdel((void **)&(*head));
}

void		ft_dlist_add(t_dlist **head, t_dlist *new)
{
	if (!head || !new)
		return ;
	if (!(*head))
	{
		*head = new;
		new->prev = NULL;
		new->next = NULL;
	}
	else
	{
		new->next = *head;
		(*head)->prev = new;
		*head = new;
		new->prev = NULL;
	}
}
