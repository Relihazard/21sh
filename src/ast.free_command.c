/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.free_command.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 09:39:14 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:39:50 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

void	ft_afree_command(t_ast *node)
{
	int		i;

	if (node == NULL)
		return ;
	if (node->type != N_COMMAND)
		return ;
	i = 0;
	if (node->node.command_node.argv)
	{
		while (node->node.command_node.argv[i])
			free(node->node.command_node.argv[i++]);
		free(node->node.command_node.argv);
	}
	i = 0;
	if (node->node.command_node.prefix)
	{
		while (node->node.command_node.prefix[i])
			free(node->node.command_node.prefix[i++]);
		free(node->node.command_node.prefix);
	}
	ft_memdel((void **)&node);
}

void	ft_afree_redirection(t_ast *node)
{
	if (node == NULL)
		return ;
	if (node->type != N_REDIRECTION)
		return ;
	ft_freeast(node->node.redir_node.mhs);
	ft_afree_redirection_node(node);
}

void	ft_afree_command_node(t_ast *node)
{
	if (node == NULL)
		return ;
	ft_afree_command(node);
}

void	ft_afree_redirection_node(t_ast *node)
{
	t_redir_node	*red;
	int				i;

	if (node == NULL)
		return ;
	if (node->type != N_REDIRECTION)
		return ;
	red = (t_redir_node *)&node->node.redir_node;
	if (red->size)
	{
		free(red->type);
		free(red->fd);
		i = 0;
		while (i < (int)red->size)
			free(red->word[i++]);
		free(red->word);
	}
	ft_memdel((void **)&node);
}

void	ft_ared_add(t_ast *node, t_redir_type type, int fd, char *word)
{
	t_redir_node	*red;

	if (node->type != N_REDIRECTION)
		return ;
	red = (t_redir_node *)&node->node.redir_node;
	red->size++;
	red->type = (t_redir_type *)ft_memrealloc(red->type,
		sizeof(t_redir_type) * red->size,
		sizeof(t_redir_type) * (red->size - 1));
	red->type[red->size - 1] = type;
	red->fd = (int *)ft_memrealloc(red->fd, sizeof(int) * red->size,
		sizeof(int) * (red->size - 1));
	red->fd[red->size - 1] = fd;
	red->word = (char **)ft_memrealloc(red->word, sizeof(char *) * red->size,
		sizeof(int) * (red->size - 1));
	red->word[red->size - 1] = ft_strdup(word);
}
