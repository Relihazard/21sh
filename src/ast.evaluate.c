/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.evaluate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/29 10:30:50 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 03:14:20 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

void	ft_aexec_command(t_command_node *node)
{
	if (node == NULL)
		return ;
	g_shell->status = ft_builtin(node->argv);
}

int		ft_evaluate(t_ast *tree)
{
	if (tree == NULL)
		return (1);
	if (tree->type == N_SEP)
		ft_aexec_sep(&tree->node.sep_node);
	else if (tree->type == N_SEPAND)
		ft_aexec_sepand(&tree->node.sepand_node);
	else if (tree->type == N_AND)
		ft_aexec_and(&tree->node.and_node);
	else if (tree->type == N_OR)
		ft_aexec_or(&tree->node.or_node);
	else if (tree->type == N_COMMAND)
		ft_aexec_command(&tree->node.command_node);
	else if (tree->type == N_PIPE)
		ft_aexec_pipe(&tree->node.pipe_node);
	else if (tree->type == N_REDIRECTION)
		ft_aexec_redirection(&tree->node.redir_node);
	return (0);
}
