/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.parse_element.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 09:11:01 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:20:14 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static int		ft_parse_redtype(t_token *token, int *fd)
{
	int						i;
	static t_parse_redtype	arr[10] = {
		{ T_DLESSDASH, R_DLESSDASH, 0 },
		{ T_DLESS, R_DLESS, 0 },
		{ T_LESSGREAT, R_LESSGREAT, 0 },
		{ T_LESSAND, R_LESSAND, 0 },
		{ T_LESS, R_LESS, 0 },
		{ T_DGREAT, R_DGREAT, 1 },
		{ T_GREATAND, R_GREATAND, 1 },
		{ T_CLOBBER, R_CLOBBER, 1 },
		{ T_GREAT, R_GREAT, 1 },
		{ 0, 0, 0 }
	};

	i = -1;
	while (arr[++i].ttype != 0)
		if (token->type == arr[i].ttype)
			break ;
	if (*fd == -1)
		*fd = arr[i].fd;
	return (arr[i].rtype);
}

void			ft_parse_redirection(t_parser *parser, t_ast **red)
{
	t_token			*token;
	int				fd;
	t_redir_type	redtype;

	fd = -1;
	if (*red == NULL)
		*red = ft_padd_node(parser, ft_acreate_redirection());
	if ((token = parser->lexer->list)->type == T_IO_NUMBER)
	{
		fd = ft_atoi(token->value);
		if (fd < 0)
			ft_parse_error(parser, token);
		token = ft_lnext_token(parser->lexer);
	}
	redtype = ft_parse_redtype(token, &fd);
	token = ft_lnext_token(parser->lexer);
	if (token->type == T_WORD)
		ft_ared_add(*red, redtype, fd, token->value);
	else
		ft_parse_error(parser, token);
}

int				ft_parse_element(t_parser *parser, t_ast *cmd, t_ast **red)
{
	t_token	*token;
	int		found;

	found = 0;
	while (42)
	{
		token = parser->lexer->list;
		if (token->type >= T_DLESSDASH && token->type <= T_IO_NUMBER)
		{
			ft_parse_redirection(parser, red);
			token = ft_lnext_token(parser->lexer);
			found++;
		}
		else if (token->type == T_WORD)
		{
			ft_acmd_addargv(cmd, token->value);
			token = ft_lnext_token(parser->lexer);
			found++;
		}
		else
			break ;
	}
	return (found);
}

t_ast			*ft_parse_andor(t_parser *parser)
{
	int		type;
	t_ast	*lhs;
	t_ast	*rhs;

	lhs = ft_parse_pipeline(parser);
	type = parser->lexer->list->type;
	if (type == T_AND || type == T_OR)
	{
		(void)ft_lnext_token(parser->lexer);
		rhs = ft_parse_andor(parser);
		if (type == T_AND)
			return (ft_padd_node(parser, ft_acreate_and(lhs, rhs)));
		else
			return (ft_padd_node(parser, ft_acreate_or(lhs, rhs)));
	}
	return (lhs);
}
