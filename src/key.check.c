/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.check.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/07 15:08:27 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:10:50 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int		ft_kcheck_ctrld(char *buffer)
{
	char	*key;

	key = "\x4\0\0\0\0\0";
	if (ft_memcmp((void *)key, (void *)buffer, 6) == 0)
		return (1);
	return (0);
}

int		ft_kcheck_home(char *buffer)
{
	char	*key;

	key = "\x1b\x5b\x48\0\0\0";
	if (ft_memcmp((void *)key, (void *)buffer, 6) == 0)
		return (1);
	return (0);
}

int		ft_kcheck_end(char *buffer)
{
	char	*key;

	key = "\x1b\x5b\x46\0\0\0";
	if (ft_memcmp((void *)key, (void *)buffer, 6) == 0)
		return (1);
	return (0);
}

t_key	*ft_key_init(t_key_type type, char *buffer)
{
	t_key	*new;

	new = (t_key *)ft_memalloc(sizeof(t_key));
	new->type = type;
	ft_memset(new->value, 0, 6);
	(void)ft_memcpy(new->value, buffer, 6);
	return (new);
}

t_key	*ft_key_get(char *buffer)
{
	int					i;
	static t_kcheck_tab	funcs[15] = {
		{ K_UP, &ft_kcheck_up },
		{ K_DOWN, &ft_kcheck_down },
		{ K_RIGHT, &ft_kcheck_right },
		{ K_LEFT, &ft_kcheck_left },
		{ K_CTRL_UP, &ft_kcheck_ctrlup },
		{ K_CTRL_DOWN, &ft_kcheck_ctrldown },
		{ K_CTRL_RIGHT, &ft_kcheck_ctrlright },
		{ K_CTRL_LEFT, &ft_kcheck_ctrleft },
		{ K_BACKSPACE, &ft_kcheck_backspace },
		{ K_ENTER, &ft_kcheck_enter },
		{ K_CTRL_D, &ft_kcheck_ctrld },
		{ K_HOME, &ft_kcheck_home },
		{ K_END, &ft_kcheck_end },
		{ K_OPT_V, &ft_kcheck_optv },
		{ K_OPT_P, &ft_kcheck_optp }
	};

	i = -1;
	while (++i < 15)
		if ((funcs[i].f)(buffer))
			return (ft_key_init(funcs[i].type, buffer));
	return (ft_key_init(K_OTHER, buffer));
}
