/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.tokenization.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/29 17:06:27 by agrossma          #+#    #+#             */
/*   Updated: 2018/10/29 23:21:55 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static void	ft_tokenizationquote(t_token **token, char *input, t_lexer **lexer)
{
	(*token)->value[(*lexer)->tcchar++] = input[(*lexer)->cchar];
	if (input[(*lexer)->cchar] == '\'' && (*lexer)->state == S_QUOTE)
	{
		(*lexer)->cchar++;
		(*lexer)->state = S_GENERAL;
	}
	else if (input[(*lexer)->cchar] == '\"' && (*lexer)->state == S_DQUOTE)
	{
		(*lexer)->cchar++;
		(*lexer)->state = S_GENERAL;
	}
	else
		(*lexer)->cchar++;
}

static int	ft_tokenizationgeneral2(t_lexer **lexer, t_token **token,
			char *input, size_t size)
{
	if (ft_isspace(input[(*lexer)->cchar]))
	{
		if ((*lexer)->tcchar > 0)
		{
			ft_addtoken(&(*lexer), *token);
			(*lexer)->tcchar = 0;
		}
		*token = NULL;
		(*lexer)->cchar++;
	}
	else if (*token && (*token)->type == T_WORD)
		(*token)->value[(*lexer)->tcchar++] = input[(*lexer)->cchar++];
	else
	{
		if ((*token = ft_inittoken(size - (*lexer)->cchar)) == NULL)
			return (1);
		(*lexer)->tcchar = 0;
	}
	return (0);
}

static int	ft_tokenizationoperator(t_lexer **lexer, t_token **token,
			char *input, size_t size)
{
	if ((*lexer)->tcchar > 0)
	{
		(*token)->type = ft_isnumber((*token)->value) && (input[(*lexer)->cchar]
			== '<' || input[(*lexer)->cchar] == '>') ? T_IO_NUMBER : T_WORD;
		ft_addtoken(&(*lexer), *token);
	}
	else if ((*lexer)->tcchar == 0 && *token)
		ft_freetoken(&(*token));
	(*lexer)->tcchar = 0;
	if ((*token = ft_inittoken(size - (*lexer)->cchar)) == NULL)
		return (1);
	(*lexer)->state = S_OPERATOR;
	(*token)->value[(*lexer)->tcchar++] = input[(*lexer)->cchar++];
	return (0);
}

static void	ft_tokenizationgeneral(t_lexer **lexer, t_token **token,
			char *input, size_t size)
{
	if ((*token) && (*lexer)->state == S_OPERATOR
		&& ft_canformoperator((*token)->value, input[(*lexer)->cchar]))
		(*token)->value[(*lexer)->tcchar++] = input[(*lexer)->cchar++];
	else if ((*token) && (*lexer)->state == S_OPERATOR
		&& !ft_canformoperator((*token)->value, input[(*lexer)->cchar]))
	{
		(*token)->type = ft_getoperator((*token)->value);
		ft_addtoken(&(*lexer), *token);
		(*lexer)->state = S_GENERAL;
		*token = NULL;
		(*lexer)->tcchar = 0;
	}
	else if ((input[(*lexer)->cchar] == '\''
		|| input[(*lexer)->cchar] == '\"') && *token)
	{
		(*lexer)->state = input[(*lexer)->cchar] == '\'' ? S_QUOTE : S_DQUOTE;
		(*token)->value[(*lexer)->tcchar++] = input[(*lexer)->cchar++];
	}
	else if (ft_isoperator(input[(*lexer)->cchar]))
		ft_tokenizationoperator(lexer, token, input, size);
	else
		ft_tokenizationgeneral2(lexer, token, input, size);
}

int			ft_tokenization(t_lexer **lexer, t_token **token,
			char *input, size_t size)
{
	while (ft_isspace(input[(*lexer)->cchar]))
		(*lexer)->cchar++;
	while ((*lexer)->cchar < size)
	{
		if ((*lexer)->state == S_GENERAL || (*lexer)->state == S_OPERATOR)
			ft_tokenizationgeneral(lexer, token, input, size);
		else if ((*lexer)->state == S_QUOTE || (*lexer)->state == S_DQUOTE)
			ft_tokenizationquote(token, input, lexer);
	}
	if ((*lexer)->state == S_OPERATOR)
		(*token)->type = ft_getoperator((*token)->value);
	if ((*lexer)->tcchar > 0)
		ft_addtoken(&(*lexer), *token);
	(*lexer)->cchar = 0;
	if ((*lexer)->state == S_GENERAL || (*lexer)->state == S_OPERATOR)
		(*lexer)->tcchar = 0;
	return (0);
}
