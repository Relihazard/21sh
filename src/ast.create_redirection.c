/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.create_redirection.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 09:41:06 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:41:36 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

t_ast	*ft_acreate_redirection(void)
{
	t_ast	*node;

	node = (t_ast *)ft_memalloc(sizeof(t_ast));
	node->type = N_REDIRECTION;
	node->node.redir_node.size = 0;
	node->node.redir_node.type = NULL;
	node->node.redir_node.fd = NULL;
	node->node.redir_node.word = NULL;
	node->node.redir_node.mhs = NULL;
	return (node);
}

void	ft_acmd_addargv(t_ast *cmd, char *argv)
{
	if (cmd->type != N_COMMAND)
		return ;
	cmd->node.command_node.argc++;
	cmd->node.command_node.argv =
		(char **)ft_memrealloc(cmd->node.command_node.argv,
		(cmd->node.command_node.argc + 1) * sizeof(char *),
		(cmd->node.command_node.argc) * sizeof(char *));
	cmd->node.command_node.argv[cmd->node.command_node.argc - 1] =
		ft_strdup(argv);
	cmd->node.command_node.argv[cmd->node.command_node.argc] = NULL;
}

t_ast	*ft_acreate_command(void)
{
	t_ast	*node;

	node = (t_ast *)ft_memalloc(sizeof(t_ast));
	node->type = N_COMMAND;
	node->node.command_node.argv = NULL;
	node->node.command_node.prefix = NULL;
	node->node.command_node.argc = 0;
	return (node);
}

t_ast	*ft_acreate_pipe(t_ast *lhs, t_ast *rhs)
{
	t_ast	*node;

	node = (t_ast *)ft_memalloc(sizeof(t_ast));
	node->type = N_PIPE;
	node->node.pipe_node.lhs = lhs;
	node->node.pipe_node.rhs = rhs;
	return (node);
}

t_ast	*ft_acreate_or(t_ast *lhs, t_ast *rhs)
{
	t_ast	*node;

	node = (t_ast *)ft_memalloc(sizeof(t_ast));
	node->type = N_OR;
	node->node.or_node.lhs = lhs;
	node->node.or_node.rhs = rhs;
	return (node);
}
