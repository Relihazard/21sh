/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.loop.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/29 10:51:05 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 08:52:41 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

t_shell *g_shell = NULL;

int			ft_loop(t_shell *shell)
{
	char		buffer[6];

	shell->cursor = (t_cursor *)ft_memalloc(sizeof(t_cursor));
	ft_init_history(shell);
	ft_init_prompt(shell);
	(void)ft_printf("%s", shell->prompt);
	shell->line = ft_strnew(1);
	shell->cursor->line = 1;
	g_shell = shell;
	while (42)
	{
		signal(SIGINT, &ft_signal_handler_shell);
		signal(SIGWINCH, &ft_signal_handler_shell);
		(void)ft_memset((void *)buffer, 0, 6);
		(void)read(STDIN_FILENO, buffer, 6);
		shell->key = ft_key_get(buffer);
		ft_key_exec(shell);
		shell->key ? ft_memdel((void **)&(shell->key)) : NULL;
	}
	return (0);
}
