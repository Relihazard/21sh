/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.ast.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/29 10:53:51 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:41:30 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

t_ast	*ft_acreate_and(t_ast *lhs, t_ast *rhs)
{
	t_ast	*node;

	node = (t_ast *)ft_memalloc(sizeof(t_ast));
	node->type = N_AND;
	node->node.and_node.lhs = lhs;
	node->node.and_node.rhs = rhs;
	return (node);
}

t_ast	*ft_acreate_sepand(t_ast *lhs, t_ast *rhs)
{
	t_ast	*node;

	node = (t_ast *)ft_memalloc(sizeof(t_ast));
	node->type = N_SEPAND;
	node->node.sepand_node.lhs = lhs;
	node->node.sepand_node.rhs = rhs;
	return (node);
}

t_ast	*ft_acreate_sep(t_ast *lhs, t_ast *rhs)
{
	t_ast	*node;

	node = (t_ast *)ft_memalloc(sizeof(t_ast));
	node->type = N_SEP;
	node->node.sep_node.lhs = lhs;
	node->node.sep_node.rhs = rhs;
	return (node);
}

void	ft_freeast_node(t_ast *node)
{
	int						i;
	static t_astfree_tab	arr[7] = {
		{ N_PIPE, &ft_afree_pipe_node },
		{ N_SEP, &ft_afree_sep_node },
		{ N_SEPAND, &ft_afree_sepand_node },
		{ N_REDIRECTION, &ft_afree_redirection_node },
		{ N_AND, &ft_afree_and_node },
		{ N_OR, &ft_afree_or_node },
		{ N_COMMAND, &ft_afree_command_node }
	};

	if (node == NULL)
		return ;
	i = -1;
	while (++i < 7)
		if (arr[i].type == node->type)
		{
			(arr[i].f)(node);
			return ;
		}
}

void	ft_freeast(t_ast *tree)
{
	int						i;
	static t_astfree_tab	arr[7] = {
		{ N_PIPE, &ft_afree_pipe },
		{ N_SEP, &ft_afree_sep },
		{ N_SEPAND, &ft_afree_sepand },
		{ N_REDIRECTION, &ft_afree_redirection },
		{ N_AND, &ft_afree_and },
		{ N_OR, &ft_afree_or },
		{ N_COMMAND, &ft_afree_command }
	};

	if (tree == NULL)
		return ;
	i = -1;
	while (++i < 7)
		if (arr[i].type == tree->type)
		{
			(arr[i].f)(tree);
			return ;
		}
}
