/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.wordexp.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 15:09:39 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 12:36:04 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static char	ft_remove_quotes(t_token *token)
{
	char	*new;
	int		i;
	size_t	len;
	char	c;

	c = token->value[0];
	new = ft_strnew(ft_strlen(token->value) - 2);
	len = ft_strlen(token->value) - 1;
	i = 0;
	while (++i < (int)len)
		new[i - 1] = token->value[i];
	ft_strdel(&(token->value));
	token->value = new;
	return (c);
}

static void	ft_expand_env(t_token *token, char **new, int *i, int *j)
{
	char	*env;
	char	*var;
	size_t	len;

	len = 0;
	var = NULL;
	while (ft_isalpha(token->value[++(*i)]))
		len++;
	env = ft_strsub(token->value, (*i) - len, len);
	if ((var = ft_getenv(env)))
	{
		*new = ft_insert((*new), var, (*j));
		*new = ft_memrealloc(*new, ft_strlen(*new) + 1,
				ft_strlen(*new) + ft_strlen(token->value) + 1);
		len = ft_strlen(var);
		while (len--)
			(*j)++;
	}
	ft_strdel(&env);
}

static void	ft_wordexp_loop(t_token *token, int *i, int *j, char **new)
{
	if (token->value[(*i)] == '\\')
	{
		(*new)[(*j)++] = token->value[++(*i)];
		(*i)++;
	}
	else if (token->value[*i] == '$')
		ft_expand_env(token, new, i, j);
	else
		(*new)[(*j)++] = token->value[(*i)++];
}

static void	ft_wordexp_internal(t_token *token)
{
	char	*new;
	int		i;
	int		j;

	if (token->value[0] == '\'' || token->value[0] == '\"')
	{
		if (ft_remove_quotes(token) == '\'')
			return ;
	}
	new = ft_strnew(ft_strlen(token->value));
	i = 0;
	j = 0;
	while (token->value[i])
		ft_wordexp_loop(token, &i, &j, &new);
	ft_strdel(&(token->value));
	token->value = new;
}

int			ft_wordexp(t_token *list)
{
	t_token	*tmp;

	if (list == NULL)
		return (1);
	tmp = list;
	while (list)
	{
		if (list->type == T_WORD)
			ft_wordexp_internal(list);
		list = list->next;
	}
	list = tmp;
	return (0);
}
