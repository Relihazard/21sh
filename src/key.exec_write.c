/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.exec_write.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 15:02:39 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 10:03:51 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static void	ft_line_write(t_shell *shell)
{
	if (shell->cursor->cchar > ft_strlen(shell->line) - 1)
	{
		if ((shell->cursor->cchar + shell->prompt_len - 1) %
			shell->screen_size.ws_col == 0)
			shell->cursor->line++;
	}
	else
	{
		if ((shell->cursor->cchar + shell->prompt_len) %
			shell->screen_size.ws_col == 0)
			shell->cursor->line++;
	}
}

static int	ft_kexec_other_copy(t_shell *shell)
{
	int		i;

	if (shell->key->value[0] == 'y' || shell->key->value[0] == 'd')
	{
		shell->key->value[0] == 'd' ?
			(void)ft_memmove(&shell->line[shell->cursor->cchar],
			&shell->line[shell->cursor->cchar + ft_strlen(shell->buf_copy)],
			ft_strlen(shell->buf_copy)) : NULL;
		i = shell->cursor->line;
		while (--i)
			(void)tputs(tgoto(tgetstr("up", NULL), 0, 0), 0, &ft_iputchar);
		(void)tputs(tgoto(tgetstr("ch", NULL), 0, 0), 0, &ft_iputchar);
		(void)tputs(tgetstr("cd", NULL), 0, &ft_iputchar);
		(void)ft_printf("%s%s", shell->prompt, shell->line);
		shell->cursor->cchar = ft_strlen(shell->line) + 1;
		shell->cursor->line =
			1 + ((ft_strlen(shell->line) + shell->prompt_len)
			/ shell->screen_size.ws_col);
		while (shell->cursor->cchar-- > shell->copy_char)
			ft_kexec_left(shell);
		shell->state = S_GENERAL;
		shell->copy_char = 0;
		return (0);
	}
	return (1);
}

int			ft_kexec_other(t_shell *shell)
{
	int		i;

	if (!ft_isprint(shell->key->value[0]))
		return (1);
	else if (shell->state == S_COPY)
		return (ft_kexec_other_copy(shell));
	shell->line = ft_insert(shell->line, (char[2]){shell->key->value[0], 0},
		shell->cursor->cchar++);
	ft_key_move_beg(shell);
	(void)ft_printf("%s%s", shell->prompt, shell->line);
	if ((shell->key->value[0] == '\'' || shell->key->value[0] == '"')
		&& shell->state == S_GENERAL)
		shell->state = shell->key->value[0] == '\'' ? S_QUOTE : S_DQUOTE;
	else if (shell->key->value[0] == '\'' && shell->state == S_QUOTE)
		shell->state = S_GENERAL;
	else if (shell->key->value[0] == '"' && shell->state == S_DQUOTE)
		shell->state = S_GENERAL;
	i = ft_strlen(shell->line);
	ft_line_write(shell);
	while (i-- > (int)shell->cursor->cchar)
		(void)tputs(tgetstr("le", NULL), 0, &ft_iputchar);
	ft_memdel((void **)&(shell->key));
	return (0);
}

static void	ft_line_backspace(t_shell *shell)
{
	if (shell->cursor->cchar > ft_strlen(shell->line) - 1)
	{
		if ((shell->cursor->cchar + shell->prompt_len) %
			shell->screen_size.ws_col == 0)
			shell->cursor->line--;
	}
	else
	{
		if ((shell->cursor->cchar + shell->prompt_len + 1) %
			shell->screen_size.ws_col == 0)
			shell->cursor->line--;
	}
}

int			ft_kexec_backspace(t_shell *shell)
{
	int		i;

	if (shell->cursor->cchar <= 0 || shell->state == S_COPY)
		return (1);
	shell->cursor->cchar--;
	(void)ft_memmove(shell->line + shell->cursor->cchar,
		shell->line + (shell->cursor->cchar + 1),
		ft_strlen(shell->line) - shell->cursor->cchar);
	ft_key_move_beg(shell);
	(void)ft_printf("%s%s", shell->prompt, shell->line);
	ft_line_backspace(shell);
	i = ft_strlen(shell->line);
	while (i-- > (int)shell->cursor->cchar)
		(void)tputs(tgetstr("le", NULL), 0, &ft_iputchar);
	ft_memdel((void **)&(shell->key));
	return (0);
}
