/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.exec_enter.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 02:16:17 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 10:28:43 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static void	ft_ngeneral(t_shell *shell)
{
	ft_strdel(&shell->prompt);
	if (shell->last_line == NULL)
	{
		shell->last_line = ft_strnew(1);
		shell->last_line = ft_insert(shell->last_line,
			shell->line, ft_strlen(shell->last_line));
	}
	if (shell->state == S_QUOTE)
	{
		shell->prompt = ft_strdup("quote> ");
		shell->prompt_len = 7;
		ft_printf("%s", "quote> ");
	}
	else if (shell->state == S_DQUOTE)
	{
		shell->prompt = ft_strdup("dquote> ");
		shell->prompt_len = 8;
		ft_printf("%s", "dquote> ");
	}
	ft_strdel(&shell->line);
	shell->line = ft_strnew(1);
}

static int	ft_check_quote(char *line)
{
	int		state;
	int		i;

	state = S_GENERAL;
	i = -1;
	while (line[++i])
	{
		if (line[i] == '\'' && state == S_QUOTE)
			state = S_GENERAL;
		else if (line[i] == '"' && state == S_DQUOTE)
			state = S_GENERAL;
		else if ((line[i] == '"' || line[i] == '\'') && state == S_GENERAL)
			state = line[i] == '"' ? S_DQUOTE : S_QUOTE;
	}
	return (state);
}

static void	ft_heregneral(t_shell *shell)
{
	ft_memdel((void **)&(shell->key));
	shell->heredoc_buf = (char *)ft_memrealloc(shell->heredoc_buf, 6,
		ft_strlen(shell->heredoc_buf));
	shell->key = ft_key_get(shell->heredoc_buf);
	ft_key_exec(shell);
	if (shell->key)
		ft_memdel((void **)&(shell->key));
	(void)ft_memset((void *)shell->heredoc_buf, 0, 6);
	ft_strdel(&shell->heredoc_buf);
}

static void	ft_prompt_general(t_shell *shell)
{
	ft_strdel(&shell->prompt);
	ft_init_prompt(shell);
	!shell->heredoc_buf ? ft_printf("%s", shell->prompt) : 0;
	shell->line = ft_strnew(1);
}

int			ft_kexec_enter(t_shell *shell)
{
	ft_printf("\n");
	if (shell->last_line)
	{
		shell->last_line = ft_insert(shell->last_line,
			shell->line, ft_strlen(shell->last_line));
		ft_strdel(&shell->line);
		shell->line = ft_strdup(shell->last_line);
		ft_strdel(&shell->last_line);
	}
	shell->state = ft_check_quote(shell->line);
	shell->state == S_GENERAL ? ft_hadd_line(shell) : 0;
	shell->state == S_GENERAL ? ft_buildlexer(shell) : 0;
	shell->cursor->cchar = 0;
	shell->state == S_GENERAL ? ft_strdel(&(shell->line)) : NULL;
	if (shell->state == S_GENERAL)
		ft_prompt_general(shell);
	else if (shell->state == S_QUOTE || shell->state == S_DQUOTE)
		ft_ngeneral(shell);
	shell->cursor->cchar = 0;
	shell->heredoc_buf ? ft_heregneral(shell) : 0;
	return (0);
}
