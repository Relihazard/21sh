/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.redirection.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 09:35:23 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:08:05 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static int				ft_apply_rgreatand(t_redir_node *node, int i)
{
	int			fd;
	char		*join;
	struct stat	s;

	fd = -1;
	if (node->word[i][0] == '-' && node->word[i][1] == 0)
		close(node->fd[i]);
	else if (ft_isnumber(node->word[i]))
	{
		fd = ft_atoi(node->word[i]);
		join = ft_strjoin("/dev/fd/", node->word[i]);
		stat(join, &s);
		free(join);
		if (!(s.st_mode & S_IWUSR))
			return (ft_red_error());
	}
	else if ((fd = open(node->word[i], O_CREAT | O_WRONLY,
		S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH)) == -1)
		return (ft_red_error());
	if (fd >= 0)
		dup2(fd, node->fd[i]);
	return (0);
}

static int				ft_apply_redirection(t_redir_node *node)
{
	int					i;
	static t_redir_tab	funcs[9] = {
		{ R_LESS, &ft_apply_rless },
		{ R_LESSAND, &ft_apply_rlessand },
		{ R_GREAT, &ft_apply_rgreat },
		{ R_GREATAND, &ft_apply_rgreatand },
		{ R_DGREAT, &ft_apply_rdgreat },
		{ R_LESSGREAT, &ft_apply_rlessgreat },
		{ R_CLOBBER, &ft_apply_rgreat },
		{ R_DLESS, &ft_apply_rdless },
		{ R_DLESSDASH, &ft_apply_rdless }
	};

	if (node == NULL)
		return (S_EXIT_FAILURE);
	i = -1;
	while (++i < (int)node->size)
	{
		if (funcs[node->type[i]].f(node, i))
			return (S_EXIT_FAILURE);
	}
	return (S_EXIT_SUCCESS);
}

static t_redir_context	*ft_save_context(t_redir_node *node)
{
	t_redir_context	*context;
	int				i;

	context = NULL;
	context = (t_redir_context *)ft_memrealloc(context,
		(node->size + 1) * sizeof(t_redir_context), 0);
	i = -1;
	while (++i < (int)node->size)
	{
		context[i].old = node->fd[i];
		context[i].save = dup(node->fd[i]);
	}
	context[i].old = -1;
	return (context);
}

static void				ft_restore_context(t_redir_context *context)
{
	int		i;

	i = -1;
	while (context[++i].old != -1)
		dup2(context[i].save, context[i].old);
	free(context);
}

void					ft_aexec_redirection(t_redir_node *node)
{
	t_redir_context	*context;

	if (node == NULL)
		return ;
	context = ft_save_context(node);
	g_shell->status = ft_apply_redirection(node);
	if (g_shell->status == S_EXIT_SUCCESS)
		ft_evaluate(node->mhs);
	ft_restore_context(context);
}
