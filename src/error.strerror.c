/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.strerror.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 11:57:29 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:12:12 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

char	*ft_strerror(int errnum)
{
	int					i;
	static t_err_tab	err[5] = {
		{ E_ACCES, "Permission denied" },
		{ E_NOENT, "No such file or directory" },
		{ E_2BIG, "Argument list too long" },
		{ E_NOTDIR, "Not a directory" },
		{ E_UNKNO, "Unknown error" }
	};

	i = -1;
	while (err[++i].errnum < 256)
		if ((err[i].errnum) == errnum)
			return (err[i].str);
	return (err[i].str);
}
