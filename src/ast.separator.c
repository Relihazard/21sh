/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.separator.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 09:27:55 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:06:12 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

void	ft_aexec_or(t_binary_node *node)
{
	if (node == NULL)
		return ;
	ft_evaluate(node->lhs);
	if (g_shell->status == S_EXIT_FAILURE)
		ft_evaluate(node->rhs);
	else
	{
		while (node->rhs->type == N_OR)
		{
			if (node->rhs->type == N_AND)
				ft_evaluate(node->rhs->node.and_node.rhs);
			node = &node->rhs->node.or_node;
		}
	}
}

void	ft_aexec_and(t_binary_node *node)
{
	if (node == NULL)
		return ;
	ft_evaluate(node->lhs);
	if (g_shell->status == S_EXIT_SUCCESS)
		ft_evaluate(node->rhs);
	else
	{
		while (node->rhs->type == N_AND)
		{
			if (node->rhs->type == N_OR)
				ft_evaluate(node->rhs->node.or_node.rhs);
			node = &node->rhs->node.and_node;
		}
	}
}

void	ft_aexec_sepand(t_binary_node *node)
{
	if (node == NULL)
		return ;
	ft_evaluate(node->lhs);
	if (node->rhs)
		ft_evaluate(node->rhs);
}

void	ft_aexec_sep(t_binary_node *node)
{
	if (node == NULL)
		return ;
	ft_evaluate(node->lhs);
	if (node->rhs)
		ft_evaluate(node->rhs);
}
