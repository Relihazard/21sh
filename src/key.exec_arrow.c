/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.exec_arrow.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 14:54:52 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:14:06 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int			ft_kexec_up(t_shell *shell)
{
	int		i;

	if (shell->history->current_id == (int)shell->history->csize
		|| shell->state == S_COPY)
	{
		(void)tputs(tgetstr("bl", NULL), 0, &ft_iputchar);
		return (1);
	}
	shell->history->list = shell->history->current_id == 0
		? shell->history->list : shell->history->list->next;
	i = shell->cursor->line;
	while (--i)
		(void)tputs(tgoto(tgetstr("up", NULL), 0, 0), 0, &ft_iputchar);
	(void)tputs(tgoto(tgetstr("ch", NULL), 0, 0), 0, &ft_iputchar);
	(void)tputs(tgetstr("cd", NULL), 0, &ft_iputchar);
	(void)ft_printf("%s%s", shell->prompt, shell->history->list->content);
	shell->cursor->cchar = ft_strlen(shell->history->list->content);
	ft_strdel(&(shell->line));
	shell->line = ft_strdup(shell->history->list->content);
	shell->history->current_id++;
	shell->cursor->line =
		1 + ((ft_strlen(shell->line) + shell->prompt_len)
		/ shell->screen_size.ws_col);
	return (0);
}

static void	ft_kexec_down_prev(t_shell *shell)
{
	int		i;

	shell->history->list = shell->history->list->prev;
	i = shell->cursor->line;
	while (--i)
		(void)tputs(tgoto(tgetstr("up", NULL), 0, 0), 0, &ft_iputchar);
	(void)tputs(tgoto(tgetstr("ch", NULL), 0, 0), 0, &ft_iputchar);
	(void)tputs(tgetstr("cd", NULL), 0, &ft_iputchar);
	ft_printf("%s%s", shell->prompt, shell->history->list->content);
	shell->cursor->cchar = ft_strlen(shell->history->list->content);
	ft_strdel(&(shell->line));
	shell->line = ft_strdup(shell->history->list->content);
	shell->cursor->line =
		1 + ((ft_strlen(shell->line) + shell->prompt_len)
		/ shell->screen_size.ws_col);
	shell->history->current_id--;
}

int			ft_kexec_down(t_shell *shell)
{
	int		i;

	if (shell->history->current_id == 0 || shell->state == S_COPY)
		(void)tputs(tgetstr("bl", NULL), 0, &ft_iputchar);
	else if (shell->history->current_id == 1)
	{
		i = shell->cursor->line;
		while (--i)
			(void)tputs(tgoto(tgetstr("up", NULL), 0, 0), 0, &ft_iputchar);
		(void)tputs(tgoto(tgetstr("ch", NULL), 0, 0), 0, &ft_iputchar);
		(void)tputs(tgetstr("cd", NULL), 0, &ft_iputchar);
		ft_printf("%s", shell->prompt);
		shell->cursor->cchar = 0;
		ft_strdel(&(shell->line));
		shell->line = ft_strnew(1);
		shell->history->current_id--;
		shell->cursor->line = 1;
	}
	else if (shell->history->list->prev)
		ft_kexec_down_prev(shell);
	return (0);
}

int			ft_kexec_right(t_shell *shell)
{
	if (shell->cursor->cchar >= ft_strlen(shell->line))
		return (1);
	else if (shell->state == S_COPY)
		return (ft_kexec_copy_right(shell));
	shell->cursor->cchar++;
	if ((shell->cursor->cchar + shell->prompt_len)
		% shell->screen_size.ws_col == 0)
	{
		(void)tputs(tgoto(tgetstr("do", NULL), 0, 0), 0, &ft_iputchar);
		(void)tputs(tgoto(tgetstr("ch", NULL), 0, 0), 0, &ft_iputchar);
		shell->cursor->line++;
	}
	else
		(void)tputs(tgetstr("nd", NULL), 0, &ft_iputchar);
	return (0);
}

int			ft_kexec_left(t_shell *shell)
{
	if (shell->cursor->cchar <= 0)
		return (1);
	else if (shell->state == S_COPY)
		return (ft_kexec_copy_left(shell));
	(void)tputs(tgetstr("le", NULL), 0, &ft_iputchar);
	shell->cursor->cchar--;
	if ((shell->cursor->cchar + shell->prompt_len)
		% shell->screen_size.ws_col == 0)
		shell->cursor->line--;
	return (0);
}
