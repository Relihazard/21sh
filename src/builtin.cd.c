/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.cd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 17:14:24 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/07 22:22:38 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static void	ft_setenv_cd(char *old)
{
	char	*new;

	new = NULL;
	new = getcwd(new, 0);
	(void)ft_bsetenv((char *[4]){"setenv", "OLDPWD", old, NULL});
	(void)ft_bsetenv((char *[4]){"setenv", "PWD", new, NULL});
	ft_strdel(&new);
}

static void	ft_change_dir(char *dir)
{
	char		*new_dir;
	char		*old_pwd;

	old_pwd = NULL;
	if (dir == NULL)
		return ;
	old_pwd = getcwd(old_pwd, 0);
	new_dir = ft_strdup(dir);
	if (!chdir(new_dir))
		ft_setenv_cd(old_pwd);
	else
	{
		if (access(new_dir, F_OK))
			ft_dprintf(2, "%s: %s: %s: %s\n",
				PROG_NAME, "cd", ft_strerror(E_NOENT), dir);
		else if (access(new_dir, X_OK))
			ft_dprintf(2, "%s: %s: %s: %s\n",
				PROG_NAME, "cd", ft_strerror(E_ACCES), dir);
		else
			ft_dprintf(2, "%s: %s: %s: %s\n",
				PROG_NAME, "cd", ft_strerror(E_NOTDIR), dir);
	}
	ft_strdel(&old_pwd);
	ft_strdel(&new_dir);
}

int			ft_bcd(char **argv)
{
	size_t	len;

	len = ft_2darraylen((void **)argv);
	if (len == 1)
		ft_change_dir(ft_getenv("HOME"));
	else if (len > 2)
	{
		ft_dprintf(2, "%s: %s: %s\n", PROG_NAME, argv[0], ft_strerror(E_2BIG));
		return (E_2BIG);
	}
	else
	{
		if (ft_strlen(argv[1]) == 1 && !ft_strncmp(argv[1], "-", 1))
			ft_change_dir(ft_getenv("OLDPWD"));
		else
			ft_change_dir(argv[1]);
	}
	return (0);
}
