/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.parser.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/10 14:19:06 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:17:16 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

void			ft_pfree(t_parser *parser)
{
	free(parser->tree);
	free(parser);
}

t_ast			*ft_padd_node(t_parser *parser, t_ast *node)
{
	if (!node)
		return (node);
	if (parser->tree_pos >= parser->tree_size)
	{
		parser->tree_size++;
		parser->tree =
			(t_ast **)ft_memrealloc(parser->tree,
			parser->tree_size * sizeof(t_ast *),
			(parser->tree_size - 1) * sizeof(t_ast *));
	}
	parser->tree[parser->tree_pos++] = node;
	return (node);
}

static t_ast	*ft_parse_list(t_parser *parser)
{
	t_ast	*lhs;
	t_ast	*rhs;
	int		type;
	int		type2;

	lhs = ft_parse_andor(parser);
	type = parser->lexer->list->type;
	if (type == T_SEMICOLON || type == T_AND)
	{
		(void)ft_lnext_token(parser->lexer);
		type2 = parser->lexer->list->type;
		if (type2 == T_NEWLINE || type2 == T_EOF)
			return (ft_padd_node(parser, (type == T_SEMICOLON ? lhs :
				ft_acreate_sepand(lhs, NULL))));
			rhs = ft_parse_list(parser);
		if (type == T_SEMICOLON)
			return (ft_padd_node(parser, ft_acreate_sep(lhs, rhs)));
		else
			return (ft_padd_node(parser, ft_acreate_sepand(lhs, rhs)));
	}
	return (lhs);
}

static t_ast	*ft_parse_input(t_parser *parser)
{
	t_token	*token;
	t_ast	*node;

	token = parser->lexer->list;
	if (token->type == T_EOF)
		return (NULL);
	if (token->type == T_NEWLINE)
	{
		token = ft_lnext_token(parser->lexer);
		return (NULL);
	}
	node = ft_parse_list(parser);
	token = ft_lnext_token(parser->lexer);
	if (token && token->type != T_EOF && token->type != T_NEWLINE
		&& token != NULL)
		ft_parse_error(parser, token);
	while (parser->lexer->list)
	{
		token = parser->lexer->list;
		parser->lexer->list = parser->lexer->list->next;
		ft_freetoken(&token);
	}
	return (node);
}

t_ast			*ft_parse(t_parser *parser)
{
	if (parser->lexer->tokenc == 0 || parser->lexer->list->value[0] == 0)
		return (NULL);
	return (ft_parse_input(parser));
}
