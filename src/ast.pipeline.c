/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.pipeline.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 09:20:34 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:05:29 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static void		ft_init_pipeline(t_pipeline *pipeline)
{
	pipeline->count = 0;
	pipeline->last = -1;
	pipeline->list = NULL;
}

static int		ft_exec_pipeline(t_pipeline *pipeline, t_binary_node *node)
{
	int		p[2];
	pid_t	pid;
	int		save;
	int		ret;

	if (pipe(p))
		return (0);
	if ((pid = ft_exec_pipe_right(p, node->rhs, pipeline)) < 0)
		return (0);
	if (node->lhs->type == N_PIPE)
	{
		save = dup(STDOUT_FILENO);
		dup2(p[1], STDOUT_FILENO);
		close(p[1]);
		ret = ft_exec_pipeline(pipeline, &node->lhs->node.pipe_node);
		dup2(save, STDOUT_FILENO);
		close(save);
		return (ret);
	}
	return (ft_exec_pipe_left(p, node->lhs, pipeline));
}

static pid_t	ft_pop_pid(t_pipeline *pipeline)
{
	if (pipeline->count <= 0 || pipeline->last + 1 >= pipeline->count)
		return (-1);
	return (pipeline->list[++pipeline->last]);
}

static void		ft_wait_controler(t_pipeline *pipeline, int killer)
{
	int		status;
	pid_t	pid;

	if (!killer)
	{
		pid = ft_pop_pid(pipeline);
		waitpid(pid, &status, 0);
		status = WEXITSTATUS(status);
		g_shell->status = WEXITSTATUS(status);
	}
	while ((pid = ft_pop_pid(pipeline)) != -1)
	{
		kill(pid, SIGPIPE);
		waitpid(pid, &status, 0);
		status = WEXITSTATUS(status);
	}
}

void			ft_aexec_pipe(t_binary_node *node)
{
	int			ret;
	t_pipeline	pipeline;

	ft_init_pipeline(&pipeline);
	ret = ft_exec_pipeline(&pipeline, node);
	if (ret == 0)
		ft_dprintf(2, PROG_NAME": Pipe error.\n");
	signal(SIGINT, &ft_signal_handler_cmd);
	ft_wait_controler(&pipeline, !ret);
	free(pipeline.list);
}
