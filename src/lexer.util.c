/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   l.util.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/29 19:02:23 by agrossma          #+#    #+#             */
/*   Updated: 2018/10/09 03:18:29 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int			ft_isoperator(char c)
{
	if (c == ';' || c == '|' || c == '&' || c == '<' || c == '>')
		return (1);
	return (0);
}

int			ft_canformoperator(char *value, char c)
{
	if (value[0] == ';')
		return (0);
	else if (ft_strlen(value) == 1)
	{
		if (value[0] == '<' && (c == '&' || c == '<' || c == '>'))
			return (1);
		else if (value[0] == '>' && (c == '&' || c == '>' || c == '|'))
			return (1);
		else if ((value[0] == '|' && c == '|') || (value[0] == '&' && c == '&'))
			return (1);
		return (0);
	}
	else if (ft_strlen(value) == 2)
	{
		if (!ft_strcmp(value, "<<"))
		{
			if (c == '-')
				return (1);
			return (0);
		}
		return (0);
	}
	else
		return (0);
}

static int	ft_getoperatorinternal(char *value)
{
	if (!ft_strncmp(value, "<<", 2))
		return (T_DLESS);
	else if (!ft_strncmp(value, ">>", 2))
		return (T_DGREAT);
	else if (!ft_strncmp(value, "<&", 2))
		return (T_LESSAND);
	else if (!ft_strncmp(value, ">&", 2))
		return (T_GREATAND);
	else if (!ft_strncmp(value, "<>", 2))
		return (T_LESSGREAT);
	else if (!ft_strncmp(value, ">|", 2))
		return (T_CLOBBER);
	else if (!ft_strncmp(value, "&&", 2))
		return (T_AND);
	else if (!ft_strncmp(value, "||", 2))
		return (T_OR);
	else if (!ft_strncmp(value, ">|", 2))
		return (T_CLOBBER);
	return (0);
}

int			ft_getoperator(char *value)
{
	if (ft_strlen(value) == 1)
	{
		if (!ft_strncmp(value, "|", 1))
			return (T_PIPE);
		else if (!ft_strncmp(value, ";", 1))
			return (T_SEMICOLON);
		else if (!ft_strncmp(value, "<", 1))
			return (T_LESS);
		else if (!ft_strncmp(value, ">", 1))
			return (T_GREAT);
		else if (!ft_strncmp(value, "&", 1))
			return (T_AMPERSAND);
	}
	else if (ft_strlen(value) == 2)
		return (ft_getoperatorinternal(value));
	else if (ft_strlen(value) == 3)
	{
		if (!ft_strncmp(value, "<<-", 3))
			return (T_DLESSDASH);
	}
	return (T_WORD);
}
