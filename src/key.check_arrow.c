/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   k.check_arrow.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 15:05:32 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:11:13 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int		ft_kcheck_up(char *buffer)
{
	static char	*arrow = NULL;

	if (arrow == NULL)
	{
		arrow = tgetstr("ku", NULL);
		arrow[1] = 91;
	}
	if (ft_memcmp((void *)arrow, (void *)buffer, ft_strlen(arrow)) == 0)
		return (1);
	return (0);
}

int		ft_kcheck_down(char *buffer)
{
	static char	*arrow = NULL;

	if (arrow == NULL)
	{
		arrow = tgetstr("kd", NULL);
		arrow[1] = 91;
	}
	if (ft_memcmp((void *)arrow, (void *)buffer, ft_strlen(arrow)) == 0)
		return (1);
	return (0);
}

int		ft_kcheck_right(char *buffer)
{
	static char	*arrow = NULL;

	if (arrow == NULL)
	{
		arrow = tgetstr("kr", NULL);
		arrow[1] = 91;
	}
	if (ft_memcmp((void *)arrow, (void *)buffer, ft_strlen(arrow)) == 0)
		return (1);
	return (0);
}

int		ft_kcheck_left(char *buffer)
{
	static char	*arrow = NULL;

	if (arrow == NULL)
	{
		arrow = tgetstr("kl", NULL);
		arrow[1] = 91;
	}
	if (ft_memcmp((void *)arrow, (void *)buffer, ft_strlen(arrow)) == 0)
		return (1);
	return (0);
}
