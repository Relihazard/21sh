/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.apply_redirection.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 09:38:07 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/10 19:06:17 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int		ft_apply_rless(t_redir_node *node, int i)
{
	int		fd;

	if ((fd = open(node->word[i], O_RDONLY)) == -1)
		return (ft_red_error());
	dup2(fd, node->fd[i]);
	close(fd);
	return (0);
}

int		ft_apply_rgreat(t_redir_node *node, int i)
{
	int		fd;

	if ((fd = open(node->word[i], O_WRONLY | O_CREAT | O_TRUNC,
		S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH)) == -1)
		return (ft_red_error());
	dup2(fd, node->fd[i]);
	close(fd);
	return (0);
}

int		ft_apply_rdgreat(t_redir_node *node, int i)
{
	int		fd;

	if ((fd = open(node->word[i], O_WRONLY | O_CREAT | O_APPEND,
		S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH)) == -1)
		return (ft_red_error());
	dup2(fd, node->fd[i]);
	close(fd);
	return (0);
}

int		ft_apply_rlessgreat(t_redir_node *node, int i)
{
	int		fd;

	if ((fd = open(node->word[i], O_CREAT | O_RDWR,
		S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH)) == -1)
		return (ft_red_error());
	dup2(fd, node->fd[i]);
	close(fd);
	return (0);
}

int		ft_apply_rlessand(t_redir_node *node, int i)
{
	int			fd;
	char		*join;
	struct stat	s;

	fd = -1;
	if (node->word[i][0] == '-' && node->word[i][1] == 0)
		close(node->fd[i]);
	else if (ft_isnumber(node->word[i]))
	{
		fd = ft_atoi(node->word[i]);
		join = ft_strjoin("/dev/fd/", node->word[i]);
		stat(join, &s);
		free(join);
		if (!(s.st_mode & S_IRUSR))
			return (ft_red_error());
	}
	else if ((fd = open(node->word[i], O_RDONLY)) == -1)
		return (ft_red_error());
	if (fd >= 0)
		dup2(fd, node->fd[i]);
	return (0);
}
