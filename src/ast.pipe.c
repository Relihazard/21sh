/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.pipe.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 09:21:33 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:04:57 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static void		ft_exec_argv(char **argv)
{
	char	*bin;

	bin = NULL;
	if (!ft_strchr(argv[0], '/'))
	{
		if ((bin = ft_get_binary(argv[0])) == NULL)
			exit(E_NOENT);
	}
	else
		bin = argv[0];
	(void)execve(bin, argv, g_environ);
	ft_strdel(&bin);
	exit(126);
}

static void		ft_execute_pipe(t_command_node *cmd)
{
	static t_builtin_tab	funcs[6] = {
		{ "cd", &ft_bcd },
		{ "echo", &ft_becho },
		{ "env", &ft_benv },
		{ "exit", &ft_bexit },
		{ "setenv", &ft_bsetenv },
		{ "unsetenv", &ft_bunsetenv }
	};
	int						i;

	if (cmd == NULL)
		return ;
	i = -1;
	while (++i < 6)
	{
		if (ft_strlen(cmd->argv[0]) == ft_strlen(funcs[i].name)
			&& ft_strcmp(cmd->argv[0], funcs[i].name) == 0)
		{
			g_shell->status = funcs[i].f(cmd->argv);
			return ;
		}
	}
	ft_exec_argv(cmd->argv);
}

static void		ft_push_pid(t_pipeline *pipeline, pid_t new)
{
	if (pipeline == NULL)
		return ;
	if (new < 0)
		return ;
	pipeline->list = (pid_t *)ft_memrealloc(pipeline->list,
		(pipeline->count + 1) * sizeof(pid_t), pipeline->count * sizeof(pid_t));
	pipeline->count++;
	pipeline->list[pipeline->count - 1] = new;
}

pid_t			ft_exec_pipe_left(int p[2], t_ast *left, t_pipeline *pipeline)
{
	int		pid;

	if ((pid = fork()) == -1)
		return (-1);
	if (pid)
	{
		close(p[1]);
		ft_push_pid(pipeline, pid);
		return (pid);
	}
	dup2(p[1], STDOUT_FILENO);
	close(p[0]);
	if (left->type == N_COMMAND)
		ft_execute_pipe(&left->node.command_node);
	else
		ft_evaluate(left);
	exit(g_shell->status);
	return (pid);
}

pid_t			ft_exec_pipe_right(int p[2], t_ast *right, t_pipeline *pipeline)
{
	int		pid;

	if ((pid = fork()) == -1)
		return (-1);
	if (pid)
	{
		close(p[0]);
		ft_push_pid(pipeline, pid);
		return (pid);
	}
	dup2(p[0], STDIN_FILENO);
	close(p[1]);
	if (right->type == N_COMMAND)
		ft_execute_pipe(&right->node.command_node);
	else
		ft_evaluate(right);
	exit(g_shell->status);
	return (pid);
}
