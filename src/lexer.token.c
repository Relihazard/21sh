/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.token.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/29 15:40:08 by agrossma          #+#    #+#             */
/*   Updated: 2018/10/29 02:17:27 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

t_token	*ft_inittoken(size_t size)
{
	t_token	*token;

	if (size == 0)
		return (NULL);
	if ((token = (t_token *)ft_memalloc(sizeof(t_token))) == NULL)
		return (NULL);
	if ((token->value = ft_strnew(size)) == NULL)
	{
		ft_memdel((void **)&token);
		return (NULL);
	}
	token->next = NULL;
	token->type = T_WORD;
	return (token);
}

void	ft_addtoken(t_lexer **head, t_token *new)
{
	t_token	*last;

	(*head)->tokenc++;
	last = (*head)->list;
	if ((*head)->list == NULL)
	{
		(*head)->list = new;
		return ;
	}
	while (last->next != NULL)
		last = last->next;
	last->next = new;
}

void	ft_freetoken(t_token **token)
{
	if (!*token || !token)
		return ;
	ft_strdel(&((*token)->value));
	ft_memdel((void **)token);
}
