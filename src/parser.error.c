/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.error.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 09:12:39 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 12:41:07 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

void		ft_parse_error(t_parser *parser, t_token *token)
{
	t_token	*tmp;

	if (parser->error)
		return ;
	if (!parser->error)
	{
		if (token->type != T_NEWLINE && token->type != T_EOF)
			ft_dprintf(2, PROG_NAME": parse error near `%s'\n", token->value);
		else
			ft_dprintf(2, PROG_NAME": parse error: unexpected end of line\n");
		parser->error = 1;
	}
	tmp = parser->lexer->list;
	while (parser->lexer->list)
	{
		parser->lexer->list->type = T_NONE;
		parser->lexer->list = parser->lexer->list->next;
	}
	parser->lexer->list = tmp;
}
