/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.exec.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/07 16:04:26 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 10:03:23 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

void	ft_key_move_beg(t_shell *shell)
{
	int		i;

	i = shell->cursor->line;
	while (--i)
		(void)tputs(tgoto(tgetstr("up", NULL), 0, 0), 0, &ft_iputchar);
	(void)tputs(tgoto(tgetstr("ch", NULL), 0, 0), 0, &ft_iputchar);
	(void)tputs(tgetstr("cd", NULL), 0, &ft_iputchar);
}

int		ft_kexec_ctrld(t_shell *shell)
{
	ft_strdel(&(shell->line));
	ft_dlist_free(&(shell->history->list));
	ft_strdel(&(shell->history->location));
	ft_memdel((void **)&(shell->history));
	ft_memdel((void **)&(shell->cursor));
	ft_memdel((void **)&(shell->key));
	ft_strdel(&(shell->buf_copy));
	ft_strdel(&(shell->prompt));
	ft_strdel(&shell->heredoc_buf);
	ft_memdel2d((void **)g_environ);
	ft_memdel((void **)&shell);
	ft_printf("exit\n");
	exit(0);
	return (0);
}

int		ft_kexec_home(t_shell *shell)
{
	if (shell->state == S_COPY)
		return (ft_kexec_copy_home(shell));
	while (!ft_kexec_left(shell))
		;
	return (0);
}

int		ft_kexec_end(t_shell *shell)
{
	if (shell->state == S_COPY)
		return (ft_kexec_copy_end(shell));
	while (!ft_kexec_right(shell))
		;
	return (0);
}

int		ft_key_exec(t_shell *shell)
{
	int					i;
	static t_kexec_tab	funcs[16] = {
		{ K_UP, &ft_kexec_up },
		{ K_DOWN, &ft_kexec_down },
		{ K_RIGHT, &ft_kexec_right },
		{ K_LEFT, &ft_kexec_left },
		{ K_CTRL_UP, &ft_kexec_ctrlup },
		{ K_CTRL_DOWN, &ft_kexec_ctrldown },
		{ K_CTRL_RIGHT, &ft_kexec_ctrlright },
		{ K_CTRL_LEFT, &ft_kexec_ctrleft },
		{ K_BACKSPACE, &ft_kexec_backspace },
		{ K_ENTER, &ft_kexec_enter },
		{ K_CTRL_D, &ft_kexec_ctrld },
		{ K_HOME, &ft_kexec_home },
		{ K_END, &ft_kexec_end },
		{ K_OPT_V, &ft_kexec_optv },
		{ K_OPT_P, &ft_kexec_optp },
		{ K_OTHER, &ft_kexec_other }
	};

	i = 0;
	while (funcs[i].type != shell->key->type)
		i++;
	return ((funcs[i].f)(shell));
}
