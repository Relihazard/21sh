/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.check_copy_paste.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 23:00:07 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/06 00:10:02 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int		ft_kcheck_optv(char *buffer)
{
	char	*key;

	key = "\x1b\x76\0\0\0\0";
	if (ft_memcmp((void *)key, (void *)buffer, 6) == 0)
		return (1);
	return (0);
}

int		ft_kcheck_optp(char *buffer)
{
	char	*key;

	key = "\x1b\x70\0\0\0\0";
	if (ft_memcmp((void *)key, (void *)buffer, 6) == 0)
		return (1);
	return (0);
}
