/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   k.check_arrow.ctrl.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 15:05:41 by agrossma          #+#    #+#             */
/*   Updated: 2018/10/09 15:06:28 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int		ft_kcheck_ctrlup(char *buffer)
{
	char	*key;

	key = "\x1b\x5b\x31\x3b\x35\x41";
	if (ft_memcmp((void *)key, (void *)buffer, 6) == 0)
		return (1);
	return (0);
}

int		ft_kcheck_ctrldown(char *buffer)
{
	char	*key;

	key = "\x1b\x5b\x31\x3b\x35\x42";
	if (ft_memcmp((void *)key, (void *)buffer, 6) == 0)
		return (1);
	return (0);
}

int		ft_kcheck_ctrlright(char *buffer)
{
	char	*key;

	key = "\x1b\x5b\x31\x3b\x35\x43";
	if (ft_memcmp((void *)key, (void *)buffer, 6) == 0)
		return (1);
	return (0);
}

int		ft_kcheck_ctrleft(char *buffer)
{
	char	*key;

	key = "\x1b\x5b\x31\x3b\x35\x44";
	if (ft_memcmp((void *)key, (void *)buffer, 6) == 0)
		return (1);
	return (0);
}
