/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.free_sepand.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 09:36:18 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:40:33 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

void	ft_afree_sepand(t_ast *node)
{
	if (node == NULL)
		return ;
	if (node->type != N_SEPAND)
		return ;
	ft_freeast(node->node.sepand_node.lhs);
	ft_freeast(node->node.sepand_node.rhs);
	ft_memdel((void **)&node);
}

void	ft_afree_sep(t_ast *node)
{
	if (node == NULL)
		return ;
	if (node->type != N_SEP)
		return ;
	ft_freeast(node->node.sep_node.lhs);
	ft_freeast(node->node.sep_node.rhs);
	ft_memdel((void **)&node);
}

void	ft_afree_and(t_ast *node)
{
	if (node == NULL)
		return ;
	if (node->type != N_AND)
		return ;
	ft_freeast(node->node.and_node.lhs);
	ft_freeast(node->node.and_node.rhs);
	ft_memdel((void **)&node);
}

void	ft_afree_or(t_ast *node)
{
	if (node == NULL)
		return ;
	if (node->type != N_OR)
		return ;
	ft_freeast(node->node.or_node.lhs);
	ft_freeast(node->node.or_node.rhs);
	ft_memdel((void **)&node);
}

void	ft_afree_pipe(t_ast *node)
{
	if (node == NULL)
		return ;
	if (node->type != N_PIPE)
		return ;
	ft_freeast(node->node.pipe_node.lhs);
	ft_freeast(node->node.pipe_node.rhs);
	ft_memdel((void **)&node);
}
