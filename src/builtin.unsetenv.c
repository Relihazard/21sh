/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.unsetenv.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/01 00:38:31 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 06:46:02 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static void	ft_remove_env(char *var)
{
	char	**new_env;
	size_t	size;
	int		i;
	int		j;

	size = ft_2darraylen((void **)g_environ);
	if (!(new_env = (char **)ft_memalloc(sizeof(char *) * size)))
		return ;
	i = 0;
	j = 0;
	while (g_environ[i])
	{
		if (!ft_strncmp(g_environ[i], var, ft_strlen(var)))
			i++;
		else
			new_env[j++] = ft_strdup(g_environ[i++]);
	}
	ft_memdel2d((void **)g_environ);
	g_environ = NULL;
	g_environ = new_env;
}

int			ft_bunsetenv(char **argv)
{
	int		i;

	if (ft_2darraylen((void **)argv) < 2)
	{
		ft_dprintf(2, "%s: %s: %s\n", PROG_NAME, argv[0], ft_strerror(E_2BIG));
		return (E_2BIG);
	}
	i = 1;
	while (argv[i])
	{
		if (ft_getenv(argv[i]))
			ft_remove_env(argv[i]);
		i++;
	}
	return (0);
}
