/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.free_sepand_node.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 09:37:34 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:40:40 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

void	ft_afree_sepand_node(t_ast *node)
{
	if (node == NULL)
		return ;
	if (node->type != N_SEPAND)
		return ;
	ft_memdel((void **)&node);
}

void	ft_afree_sep_node(t_ast *node)
{
	if (node == NULL)
		return ;
	if (node->type != N_SEP)
		return ;
	ft_memdel((void **)&node);
}

void	ft_afree_and_node(t_ast *node)
{
	if (node == NULL)
		return ;
	if (node->type != N_AND)
		return ;
	ft_memdel((void **)&node);
}

void	ft_afree_or_node(t_ast *node)
{
	if (node == NULL)
		return ;
	if (node->type != N_OR)
		return ;
	ft_memdel((void **)&node);
}

void	ft_afree_pipe_node(t_ast *node)
{
	if (node == NULL)
		return ;
	if (node->type != N_PIPE)
		return ;
	ft_memdel((void **)&node);
}
