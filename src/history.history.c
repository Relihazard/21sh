/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.history.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/07 02:42:55 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:09:05 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static int	ft_hadd_line_trunc(t_shell *shell)
{
	int		fd;
	t_dlist	*tmp;

	while (shell->history->csize >= HISTORY_MAXSIZE && shell->history->list)
	{
		ft_dlist_free_last(&(shell->history->list));
		shell->history->csize--;
	}
	if ((fd = open(shell->history->location, O_WRONLY | O_TRUNC)) == -1)
		return (1);
	ft_dlist_add(&(shell->history->list),
		ft_dlist_new(shell->line, ft_strlen(shell->line) + 1));
	shell->history->csize++;
	tmp = shell->history->list;
	while (tmp->next)
		tmp = tmp->next;
	while (tmp->prev)
	{
		ft_dprintf(fd, "%s\n", tmp->content);
		tmp = tmp->prev;
	}
	ft_dprintf(fd, "%s\n", shell->line);
	close(fd);
	return (0);
}

static int	ft_isstr(char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (ft_isalpha(str[i]))
			return (0);
		i++;
	}
	return (1);
}

int			ft_hadd_line(t_shell *shell)
{
	int			fd;

	if (HISTORY_MAXSIZE == 0)
		return (1);
	if (ft_isstr(shell->line))
		return (1);
	while (shell->history->list && shell->history->list->prev)
		shell->history->list = shell->history->list->prev;
	if (shell->history->csize >= HISTORY_MAXSIZE)
		return (ft_hadd_line_trunc(shell));
	shell->history->current_id = 0;
	if ((fd = open(shell->history->location, O_WRONLY | O_APPEND)) == -1)
		return (1);
	ft_dlist_add(&(shell->history->list),
		ft_dlist_new(shell->line, ft_strlen(shell->line) + 1));
	shell->history->csize++;
	ft_dprintf(fd, "%s\n", shell->line);
	close(fd);
	return (0);
}

static void	ft_create_location(t_history *history)
{
	char	*buf;
	char	*join;

	buf = NULL;
	buf = getcwd(buf, 0);
	join = ft_strjoin(buf, "/"HISTORY_FILE);
	ft_strdel(&buf);
	history->location = join;
}

int			ft_init_history(t_shell *shell)
{
	int			fd;
	char		*line;

	if ((shell->history = (t_history *)ft_memalloc(sizeof(t_history))) == NULL
		|| HISTORY_MAXSIZE == 0)
		return (1);
	ft_create_location(shell->history);
	if ((fd = open(shell->history->location, O_RDONLY | O_CREAT,
			S_IRUSR | S_IWUSR)) == -1)
	{
		ft_dprintf(2, "%s\n", "Could not initialize history");
		return (1);
	}
	while (get_next_line(fd, &line))
	{
		ft_dlist_add(&(shell->history->list),
			ft_dlist_new(line, ft_strlen(line) + 1));
		shell->history->csize++;
		ft_strdel(&line);
		if (shell->history->csize == HISTORY_MAXSIZE)
			break ;
	}
	close(fd);
	return (0);
}
