/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.lexer.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/29 15:30:46 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 10:38:30 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

t_token			*ft_lnext_token(t_lexer *lexer)
{
	t_token	*token;

	token = lexer->list;
	if (token == NULL || token->next == NULL)
		return (NULL);
	lexer->list = lexer->list->next;
	ft_freetoken(&token);
	token = lexer->list;
	return (token);
}

static t_token	*ft_getlasttoken(t_token *token)
{
	t_token	*tmp;

	tmp = token;
	while (tmp->next)
		tmp = tmp->next;
	return (tmp);
}

void			ft_freelexer(t_lexer **lexer)
{
	t_token	*tmp;

	if (!lexer || !*lexer)
		return ;
	while ((*lexer)->list)
	{
		tmp = (*lexer)->list;
		(*lexer)->list = (*lexer)->list->next;
		ft_freetoken(&tmp);
	}
	ft_memdel((void **)lexer);
}

static void		ft_add_nl(t_shell *shell)
{
	t_token	*token;

	if (ft_getlasttoken(shell->lexer->list)->type != T_NEWLINE ||
		ft_getlasttoken(shell->lexer->list)->type != T_EOF)
	{
		token = ft_inittoken(1);
		token->value[0] = '\n';
		token->type = T_NEWLINE;
		ft_addtoken(&(shell->lexer), token);
	}
}

int				ft_buildlexer(t_shell *shell)
{
	t_token		*token;

	if ((shell->lexer = (t_lexer *)ft_memalloc(sizeof(t_lexer))) == NULL)
		return (1);
	token = ft_inittoken(ft_strlen(shell->line));
	token ? ft_tokenization(&(shell->lexer), &token, shell->line,
		ft_strlen(shell->line)) : 0;
	shell->lexer->state = S_GENERAL;
	if (shell->lexer->tokenc == 0)
	{
		ft_freetoken(&token);
		ft_freelexer(&(shell->lexer));
		return (1);
	}
	ft_add_nl(shell);
	ft_wordexp(shell->lexer->list);
	shell->parser = (t_parser *)ft_memalloc(sizeof(t_parser));
	shell->parser->lexer = shell->lexer;
	shell->ast = ft_parse(shell->parser);
	ft_freelexer(&(shell->lexer));
	shell->parser->error == 0 ? ft_evaluate(shell->ast) : 0;
	ft_pfree(shell->parser);
	ft_freeast(shell->ast);
	return (0);
}
