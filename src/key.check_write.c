/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   k.check_write.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 15:05:47 by agrossma          #+#    #+#             */
/*   Updated: 2018/10/09 15:06:40 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int		ft_kcheck_backspace(char *buffer)
{
	char	*key;

	key = "\x7f\0\0\0\0\0";
	if (ft_memcmp((void *)key, (void *)buffer, 6) == 0)
		return (1);
	return (0);
}

int		ft_kcheck_enter(char *buffer)
{
	char	*key;

	key = "\x0a\0\0\0\0\0";
	if (ft_memcmp((void *)key, (void *)buffer, 6) == 0)
		return (1);
	return (0);
}
