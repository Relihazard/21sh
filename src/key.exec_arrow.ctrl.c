/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.exec_arrow.ctrl.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 14:56:30 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/06 14:06:23 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int		ft_kexec_ctrlright(t_shell *shell)
{
	size_t	len;

	if (shell->state == S_COPY)
		return (ft_kexec_copy_ctrlright(shell));
	len = ft_strlen(shell->line);
	while (shell->cursor->cchar < len
		&& !ft_isspace((shell->line)[shell->cursor->cchar]))
		ft_kexec_right(shell);
	while (shell->cursor->cchar < len
		&& ft_isspace((shell->line)[shell->cursor->cchar]))
		ft_kexec_right(shell);
	ft_memdel((void **)&(shell->key));
	return (0);
}

int		ft_kexec_ctrleft(t_shell *shell)
{
	if (shell->state == S_COPY)
		return (ft_kexec_copy_ctrleft(shell));
	while (shell->cursor->cchar > 0
		&& !ft_isspace((shell->line)[shell->cursor->cchar]))
		ft_kexec_left(shell);
	while (shell->cursor->cchar > 0
		&& ft_isspace((shell->line)[shell->cursor->cchar]))
		ft_kexec_left(shell);
	while (shell->cursor->cchar > 0
		&& !ft_isspace((shell->line)[shell->cursor->cchar]))
		ft_kexec_left(shell);
	if (shell->cursor->cchar > 0)
		ft_kexec_right(shell);
	ft_memdel((void **)&(shell->key));
	return (0);
}

int		ft_kexec_ctrlup(t_shell *shell)
{
	int		i;
	int		j;

	if (shell->state == S_COPY)
		return (ft_kexec_copy_ctrlup(shell));
	i = shell->cursor->cchar;
	j = shell->cursor->cchar;
	if (j >= shell->screen_size.ws_col)
		j -= shell->screen_size.ws_col;
	else
		j = 0;
	while (i-- > j)
		ft_kexec_left(shell);
	ft_memdel((void **)&(shell->key));
	return (0);
}

int		ft_kexec_ctrldown(t_shell *shell)
{
	size_t	len;
	int		i;
	int		j;

	if (shell->state == S_COPY)
		return (ft_kexec_copy_ctrldown(shell));
	i = shell->cursor->cchar;
	j = shell->cursor->cchar;
	len = ft_strlen(shell->line);
	if (len >= shell->screen_size.ws_col)
		j += shell->screen_size.ws_col;
	else
		j = len;
	while (i++ < j)
		ft_kexec_right(shell);
	ft_memdel((void **)&(shell->key));
	return (0);
}
