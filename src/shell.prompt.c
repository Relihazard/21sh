/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s.prompt.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:21:45 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/11 19:39:42 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int		ft_init_prompt(t_shell *shell)
{
	char	*user;
	char	*host;
	char	*prompt;

	if ((user = ft_getenv("USER")) == NULL
		|| ((host = ft_getenv("NAME")) == NULL
		&& (host = ft_getenv("HOST")) == NULL))
	{
		shell->prompt = ft_strdup("$> ");
		shell->prompt_len = ft_strlen(shell->prompt);
		return (1);
	}
	prompt = ft_strnew(ft_strlen(user) + ft_strlen(host) + 5);
	ft_strcat(prompt, user);
	ft_strcat(prompt, "@");
	ft_strcat(prompt, host);
	ft_strcat(prompt, " $> ");
	shell->prompt = ft_strdup(prompt);
	shell->prompt_len = ft_strlen(shell->prompt);
	ft_strdel(&prompt);
	return (0);
}
