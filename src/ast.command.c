/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.command.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 09:29:06 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:03:55 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static int	ft_check_perm(char **path, char **bin, char *argv, int i)
{
	char	*join;

	join = ft_strjoin(path[i], "/");
	*bin = ft_strjoin(join, argv);
	ft_strdel(&join);
	if (!access(*bin, F_OK))
	{
		if (!access(*bin, X_OK))
		{
			ft_memdel2d((void **)path);
			return (0);
		}
		ft_dprintf(2, "%s: %s: %s\n", PROG_NAME, ft_strerror(E_ACCES), argv);
		return (1);
	}
	ft_strdel(bin);
	return (2);
}

char		*ft_get_binary(char *argv)
{
	char	**path;
	char	*bin;
	int		i;
	int		perm;

	path = ft_strsplit(ft_getenv("PATH"), ':');
	i = -1;
	perm = 2;
	while (path && path[++i])
	{
		perm = ft_check_perm(path, &bin, argv, i);
		if (perm == 0)
			return (bin);
		else if (perm == 1)
			break ;
	}
	perm == 2 ? ft_dprintf(2, "%s: %s: %s\n",
		PROG_NAME, "Command not found", argv) : 0;
	ft_memdel2d((void **)path);
	return (NULL);
}

static char	*ft_check_argv(char *argv)
{
	struct stat	s;

	if (stat(argv, &s))
	{
		ft_dprintf(2, "%s: %s: %s\n", PROG_NAME,
			"No such file or directory", argv);
		return (NULL);
	}
	else if (!(s.st_mode & S_IXOTH))
	{
		ft_dprintf(2, "%s: %s: %s\n", PROG_NAME, "Permission denied", argv);
		return (NULL);
	}
	else if (S_ISDIR(s.st_mode))
	{
		ft_dprintf(2, "%s: %s: %s\n", PROG_NAME, argv, "Is a directory");
		return (NULL);
	}
	return (argv);
}

static int	ft_execute(char **argv)
{
	pid_t	cpid;
	int		status;
	char	*bin;

	bin = NULL;
	if (!ft_strchr(argv[0], '/'))
	{
		if ((bin = ft_get_binary(argv[0])) == NULL)
			return (1);
	}
	else if ((bin = ft_check_argv(argv[0])) == NULL)
		return (1);
	if ((cpid = fork()) == -1)
	{
		ft_dprintf(2, PROG_NAME": Cannot fork\n");
		return (126);
	}
	signal(SIGINT, &ft_signal_handler_cmd);
	if (cpid == 0)
		(void)execve(bin, argv, g_environ);
	(void)waitpid(cpid, &status, 0);
	!ft_strchr(argv[0], '/') ? ft_strdel(&bin) : NULL;
	if (WIFEXITED(status))
		return (WEXITSTATUS(status) == 0 ? S_EXIT_SUCCESS : S_EXIT_FAILURE);
	return (WTERMSIG(status));
}

int			ft_builtin(char **argv)
{
	int						i;
	static t_builtin_tab	funcs[6] = {
		{ "cd", &ft_bcd },
		{ "echo", &ft_becho },
		{ "env", &ft_benv },
		{ "exit", &ft_bexit },
		{ "setenv", &ft_bsetenv },
		{ "unsetenv", &ft_bunsetenv }
	};

	i = -1;
	while (++i < 6)
	{
		if (ft_strlen(argv[0]) == ft_strlen(funcs[i].name)
			&& ft_strcmp(argv[0], funcs[i].name) == 0)
			return (funcs[i].f(argv));
	}
	return (ft_execute(argv));
}
