/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.parse_pipeline.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 09:09:17 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 09:17:42 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static int		ft_parse_prefix(t_parser *parser, t_ast *cmd, t_ast **red)
{
	int		found;
	t_token	*token;

	found = 0;
	(void)cmd;
	while (42)
	{
		token = parser->lexer->list;
		if (token->type >= T_DLESSDASH && token->type <= T_IO_NUMBER)
		{
			ft_parse_redirection(parser, red);
			found++;
		}
		else
			break ;
	}
	return (found);
}

static t_ast	*ft_parse_simplecommand(t_parser *parser)
{
	t_ast	*cmd;
	t_ast	*red;
	int		found;

	red = NULL;
	found = 0;
	cmd = ft_padd_node(parser, ft_acreate_command());
	found += ft_parse_prefix(parser, cmd, &red);
	found += ft_parse_element(parser, cmd, &red);
	if (!found)
	{
		ft_parse_error(parser, parser->lexer->list);
		return (NULL);
	}
	if (red)
	{
		red->node.redir_node.mhs = cmd;
		cmd = red;
	}
	return (cmd);
}

static t_ast	*ft_parse_command(t_parser *parser)
{
	t_token	*token;

	token = parser->lexer->list;
	if (token->type >= T_DLESSDASH && token->type <= T_WORD)
		return (ft_parse_simplecommand(parser));
	else
		ft_parse_error(parser, token);
	return (NULL);
}

static t_ast	*ft_parse_pipeline_command(t_parser *parser)
{
	t_token *token;
	t_ast	*lhs;

	if ((token = parser->lexer->list)->type != T_PIPE)
		ft_parse_error(parser, token);
	token = ft_lnext_token(parser->lexer);
	lhs = ft_parse_command(parser);
	if ((token = parser->lexer->list)->type == T_PIPE)
		return (ft_padd_node(parser,
			ft_acreate_pipe(lhs, ft_parse_pipeline_command(parser))));
		return (lhs);
}

t_ast			*ft_parse_pipeline(t_parser *parser)
{
	t_token		*token;
	t_ast		*node;
	t_ast		*lhs;

	lhs = ft_parse_command(parser);
	if ((token = parser->lexer->list)->type == T_PIPE)
		node = ft_padd_node(parser,
			ft_acreate_pipe(lhs, ft_parse_pipeline_command(parser)));
	else
		node = lhs;
	return (node);
}
