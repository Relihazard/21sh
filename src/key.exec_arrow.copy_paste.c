/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.exec_arrow.copy_paste.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 10:07:37 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 10:08:10 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

static void		ft_copy_addremove(t_shell *shell, int t)
{
	if (t == 0)
	{
		if (shell->cursor->cchar >= shell->copy_char)
			shell->buf_copy = ft_insert(shell->buf_copy,
				(char[2]){shell->line[shell->cursor->cchar], 0},
				ft_strlen(shell->buf_copy));
		else
			(void)ft_memmove(&shell->buf_copy[0],
				&shell->buf_copy[1], ft_strlen(shell->buf_copy));
	}
	else
	{
		if (shell->cursor->cchar <= shell->copy_char)
			shell->buf_copy = ft_insert(shell->buf_copy,
				(char[2]){shell->line[shell->cursor->cchar - 1], 0}, 0);
		else
			shell->buf_copy[shell->cursor->cchar - shell->copy_char - 1] = 0;
	}
}

int				ft_kexec_copy_right(t_shell *shell)
{
	int		remind;

	if (shell->cursor->cchar >= ft_strlen(shell->line))
		return (1);
	ft_copy_addremove(shell, 0);
	shell->cursor->cchar++;
	ft_key_move_beg(shell);
	(void)ft_printf("%s", shell->prompt);
	remind = 0;
	while (remind < (int)(shell->copy_char < shell->cursor->cchar
		? shell->copy_char : shell->cursor->cchar))
		write(1, &shell->line[remind++], 1);
	(void)tputs(tgetstr("mr", NULL), 0, &ft_iputchar);
	(void)ft_printf(shell->buf_copy);
	(void)tputs(tgetstr("me", NULL), 0, &ft_iputchar);
	(void)ft_printf(shell->line + (shell->cursor->cchar < shell->copy_char
		? shell->copy_char : shell->cursor->cchar));
	remind = ft_strlen(shell->line);
	while (remind-- > (int)shell->cursor->cchar)
		(void)tputs(tgetstr("le", NULL), 0, &ft_iputchar);
	ft_memdel((void **)&(shell->key));
	return (0);
}

int				ft_kexec_copy_left(t_shell *shell)
{
	int		remind;

	if (shell->cursor->cchar <= 0)
		return (1);
	ft_copy_addremove(shell, 1);
	shell->cursor->cchar--;
	ft_key_move_beg(shell);
	(void)ft_printf("%s", shell->prompt);
	remind = 0;
	while (remind < (int)(shell->cursor->cchar
		< shell->copy_char ? shell->cursor->cchar : shell->copy_char))
		write(1, &shell->line[remind++], 1);
	(void)tputs(tgetstr("mr", NULL), 0, &ft_iputchar);
	(void)ft_printf(shell->buf_copy);
	(void)tputs(tgetstr("me", NULL), 0, &ft_iputchar);
	(void)ft_printf(shell->line + (shell->copy_char <= shell->cursor->cchar
		? shell->cursor->cchar : shell->copy_char));
	remind = ft_strlen(shell->line);
	while (remind-- > (int)shell->cursor->cchar)
		(void)tputs(tgetstr("le", NULL), 0, &ft_iputchar);
	ft_memdel((void **)&(shell->key));
	return (0);
}
