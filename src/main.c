/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/29 15:17:26 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 06:45:57 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

char	**g_environ = NULL;

int		main(int ac, char **av, char **ev)
{
	t_shell	*shell;

	(void)ac;
	(void)av;
	if ((shell = ft_memalloc(sizeof(t_shell))) == NULL)
		return (EXIT_FAILURE);
	if (ft_termios() || ft_screen(shell))
	{
		ft_putendl_fd(PROG_NAME": Could not initialize the terminal", 2);
		return (EXIT_FAILURE);
	}
	ft_initenv(ev);
	ft_loop(shell);
	return (EXIT_SUCCESS);
}
