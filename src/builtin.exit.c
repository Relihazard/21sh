/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.exit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 00:04:00 by agrossma          #+#    #+#             */
/*   Updated: 2018/11/19 12:49:26 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh21.h"

int		ft_bexit(char **argv)
{
	int		ret;

	ret = argv[1] ? ft_atoi(argv[1]) : 0;
	ft_memdel2d((void **)g_environ);
	ft_dlist_free(&(g_shell->history->list));
	ft_strdel(&(g_shell->history->location));
	ft_memdel((void **)&(g_shell->history));
	ft_memdel((void **)&(g_shell->cursor));
	ft_memdel((void **)&(g_shell->key));
	ft_strdel(&(g_shell->buf_copy));
	ft_strdel(&(g_shell->prompt));
	ft_pfree(g_shell->parser);
	ft_freeast(g_shell->ast);
	ft_memdel((void **)&g_shell);
	exit(ret);
	return (ret);
}
